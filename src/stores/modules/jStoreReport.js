/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import apiReport from '@/services/jApiReport';

const state = {
  // Set default state(s) here
  list_options: {
    date: [
      {
        text: 'Last Activity Date',
        value: 1,
      },
      {
        text: 'Assigned Date',
        value: 2,
      },
      {
        text: 'Created Date',
        value: 3,
      },
    ],
    sales: [],
    stage: [],
    activity_type: [],
    product: [],
    product_type: [],
    lead: [],
    status: [],
  },
  url: '',
  data_changed: false,
};

const getters = {

};

const mutations = {
  setUrl(state, param) {
    state.url = param.data;
  },
  setDefaultUrl(state) {
    state.url = '';
  },
  setDataChanged(state, param) {
    state.data_changed = param;
  },
};

const actions = {
  // async getMyCalendar({ commit, dispatch }, payload) {
  //   dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
  //   try {
  //     const getEvents = await apiReport.getMyCalendar(payload);
  //     commit('setMyCalendar', getEvents.data);
  //     dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
  //   } catch (err) {
  //     dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
  //  dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
  //   }
  // },
  async initListOptions({ /* commit, */dispatch }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      // const salesInCharge = await apiReport.getSalesInChargeList();
      // commit('setSalesInCharge', salesInCharge.data);
      // const stage = await apiReport.getStageList();
      // commit('setStage', stage.data);
      // const activityType = await apiReport.getActivityTypeList();
      // commit('setActivityType', activityType.data);
      // const product = await apiReport.getProductList();
      // commit('setProduct', product.data);
      // const productType = await apiReport.getProductTypeList();
      // commit('setProductType', productType.data);
      // const lead = await apiReport.getLeadList();
      // commit('setLead', lead.data);
      // const status = await apiReport.getStatusList();
      // commit('setStatus', status.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async getReportActivity({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const activity = await apiReport.getReportActivity(payload);
      commit('setUrl', activity.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async getReportOpportunity({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const opportunity = await apiReport.getReportOpportunity(payload);
      commit('setUrl', opportunity.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async getReportLead({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const lead = await apiReport.getReportLead(payload);
      commit('setUrl', lead.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  setDefaultDataChanged({ commit }) {
    commit('setDataChanged', false);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
