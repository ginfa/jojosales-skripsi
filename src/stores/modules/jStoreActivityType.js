/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import apiActivityType from '@/services/jApiActivityType';
import apiPolicy from '@/services/jApiPolicy';
import apiReminder from '@/services/jApiReminder';

const state = {
  // Set default state(s) here
  activity_type_state: {
    policy_options: [],
    reminder_options: [],
    activity_type_detail: null,
  },
  data_changed: false,
};

const getters = {

};

const mutations = {
  setActivityTypeDetail(state, payload) {
    state.activity_type_state.activity_type_detail = payload.data;
  },
  setPolicyOptions(state, payload) {
    state.activity_type_state.policy_options = [];
    payload.data.forEach((item) => {
      const option = {
        id: item.id,
        name: item.name,
      };
      const options = state.activity_type_state.policy_options;
      if (options.findIndex(x => x.name === option.name) === -1) {
        options.push(option);
      }
    });
  },
  setReminderOptions(state, payload) {
    state.activity_type_state.reminder_options = [];
    payload.data.forEach((item) => {
      const option = {
        id: item.id,
        name: item.name,
      };
      const options = state.activity_type_state.reminder_options;
      if (options.findIndex(x => x.name === option.name) === -1) {
        options.push(option);
      }
    });
  },
  setDataChanged(state, param) {
    state.data_changed = param;
  },
};

const actions = {
  async getPolicyOptions({ commit, dispatch }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const policyList = await apiPolicy.getPolicyList();
      commit('setPolicyOptions', policyList.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async getReminderOptions({ commit, dispatch }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const reminderList = await apiReminder.getReminderList();
      commit('setReminderOptions', reminderList.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async getActivityTypeDetail({ commit, dispatch }, reqId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const getDetail = await apiActivityType.getActivityTypeDetail(reqId);
      commit('setActivityTypeDetail', getDetail);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async createActivityType({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const createdActivityType = await apiActivityType.createActivityType(payload);
      commit('setDataChanged', true);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', createdActivityType.data.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async updateActivityType({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const updatedActivityType = await apiActivityType.updateActivityType(payload);
      commit('setActivityTypeDetail', updatedActivityType);
      commit('setDataChanged', true);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async deleteActivityType({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      await apiActivityType.deleteActivityType(payload);
      commit('setDataChanged', true);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  setDefaultDataChanged({ commit }) {
    commit('setDataChanged', false);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
