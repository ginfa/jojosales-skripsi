/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import apiHome from '@/services/jApiHome';
import apiStage from '@/services/jApiStage';
import constant from '@/constant/constant';

const state = {
  // Set default state(s) here
  home_state: {
    opportunity: {
      data: [
        {
          title: 'Presales',
          value: 5000000,
          color: '#42A5CE',
          detail: [
            {
              date: '2019-01-01',
              value: 1000000,
            },
            {
              date: '2019-01-08',
              value: 2000000,
            },
          ],
        },
        {
          title: 'Postsales',
          value: 5000000,
          color: '#D2232A',
          detail: [
            {
              date: '2019-01-01',
              value: 1000000,
            },
            {
              date: '2019-01-08',
              value: 2000000,
            },
          ],
        },
      ],
      selected: [],
    },
    win_lost: {
      data: [
        {
          title: 'Deals',
          value: 5000000,
          detail: [
            {
              date: '2019-01-01',
              value: 1000000,
            },
            {
              date: '2019-01-08',
              value: 2000000,
            },
          ],
        },
        {
          title: 'Lost',
          value: 5000000,
          detail: [
            {
              date: '2019-01-01',
              value: 1000000,
            },
            {
              date: '2019-01-08',
              value: 2000000,
            },
          ],
        },
      ],
      selected: [],
    },
    leaderboard: {
      stage_list: [],
      current_stage: null,
      activity_type_options: [],
      list: [],
    },
    activity_detail: [],
  },
  data_changed: false,
};

const getters = {

};

const mutations = {
  setOpportunityGraph(state, param) {
    state.home_state.opportunity.data = param.data;
  },
  setWinLostGraph(state, param) {
    state.home_state.win_lost.data = param.data;
  },
  setActivityDetail(state, payload) {
    payload.data.forEach((instance) => {
      const d = new Date(instance.start_date);

      let minutes = `${d.getMinutes()}`;

      if (minutes.length < 2) minutes = `0${minutes}`;
      const period = (d.getHours() > 12) ? ' PM' : ' AM';
      const hours = (d.getHours() > 12) ? d.getHours() - 12 : d.getHours();

      const time = `${hours}:${minutes}${period}`;

      const activity = {
        icon: instance.stage_icon,
        name: instance.opportunity_name,
        time,
        activity_name: instance.activity_type_name,
        address: instance.address,
        stage: instance.stage_name,
        color: instance.stage_color,
      };
      state.home_state.activity_detail.push(activity);
    });
  },
  setSelectedOpportunityGraph(state, index) {
    state.home_state.opportunity.selected = [];
    state.home_state.opportunity.selected.push(state.home_state.opportunity.data[index]);
  },
  setSelectedWinLostGraph(state, index) {
    const data = state.home_state.win_lost.data[index];
    data.index = index;
    state.home_state.win_lost.selected = [];
    state.home_state.win_lost.selected.push(data);
  },
  setStageList(state, param) {
    state.home_state.leaderboard.stage_list = [];
    state.home_state.leaderboard.activity_type_options = [];
    param.data.forEach((instance) => {
      const data = {
        id: instance.id,
        status: instance.name,
        summaryDate: '-',
        type: instance.type,
        isStart: false,
        isEnd: false,
        color: constant.configureColor(instance.color),
        active: false,
        href: '',
        activity_type: instance.activity_type,
      };
      state.home_state.leaderboard.stage_list.push(data);
    });
    state.home_state.leaderboard.stage_list[0].isStart = true;
    const lastIdx = state.home_state.leaderboard.stage_list.length - 1;
    state.home_state.leaderboard.stage_list[lastIdx].isEnd = true;
    state.home_state.leaderboard.current_stage = state.home_state.leaderboard.stage_list[0];
  },
  configureStage(state, data) {
    state.home_state.leaderboard.stage_list.forEach((stage) => {
      stage.active = false;
    });
    state.home_state.leaderboard.list = [];
    state.home_state.leaderboard.activity_type_options = [];
    data.activity_type.forEach((type) => {
      state.home_state.leaderboard.activity_type_options.push(type);
    });
    data.active = true;
    state.home_state.leaderboard.current_stage = data;
  },
  setLeaderboard(state, param) {
    param.data.forEach((instance) => {
      const data = {
        name: `${instance.user.profile.first_name} ${instance.user.profile.last_name}`,
        position: instance.user.position,
        photo_url: instance.user.profile.photo_url,
        this_week: instance.this_week,
        last_week: instance.last_week,
      };

      state.home_state.leaderboard.list.push(data);
    });
  },
  setDataChanged(state, param) {
    state.data_changed = param;
  },
};

const actions = {
  async getOpportunityGraph({ commit, dispatch }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const graph = await apiHome.getOpportunityGraph();
      commit('setOpportunityGraph', graph.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },
  async getWinLostGraph({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const graph = await apiHome.getWinLostGraph(payload);
      commit('setWinLostGraph', graph.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },
  async getStageList({ commit, dispatch }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const stageList = await apiStage.getStageList();
      commit('setStageList', stageList.data);
      commit('setDataChanged', true);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },
  async getLeaderboard({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const leaderboard = await apiHome.getLeaderboard(payload);
      commit('setLeaderboard', leaderboard.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },
  async getActivityDetail({ commit, dispatch }, reqId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const getActivity = await apiHome.getActivityDetail(reqId);
      commit('setActivityDetail', getActivity.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.message, { root: true });
    }
  },
  setDefaultDataChanged({ commit }) {
    commit('setDataChanged', false);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
