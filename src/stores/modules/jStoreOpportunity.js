/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import apiOpportunity from '@/services/jApiOpportunity';

const state = {
  // Set default state(s) here
  opportunity_state: {
    opportunity_detail: null,
    opportunity_list: [],
  },
  data_changed: false,
};

const getters = {

};

const mutations = {
  setOpportunityDetail(state, payload) {
    state.opportunity_state.opportunity_detail = payload.data;
  },
  setOpportunityDataTable(state, payload) {
    state.opportunity_state.opportunity_list = [];
    payload.data.forEach((element) => {
      element.pic_list = [element.activity.pic[0].detail.name];
      element.product_list = [element.activity.product[0].name];

      state.opportunity_state.opportunity_list.push(element);
    });
  },
  setDataChanged(state, param) {
    state.data_changed = param;
  },
};

const actions = {
  async getOpportunityDetail({ commit, dispatch }, reqId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const getDetail = await apiOpportunity.getOpportunityDetail(reqId);
      commit('setOpportunityDetail', getDetail.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async getOpportunityByLead({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const getDataTable = await apiOpportunity.getOpportunityByLead(payload);
      commit('setOpportunityDataTable', getDataTable.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  setDefaultDataChanged({ commit }) {
    commit('setDataChanged', false);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
