/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import jApiLeads from '../../services/jApiLeads';

const state = {
  // Set default state(s) here
  leads_state: {
    leads_detail: null,
    leads_list: null,
  },
  source_options: [],
  group_options: [],
  industry_options: [],
  country_options: [],
  province_options: [],
  city_options: [],
  data_changed: false,
};

const getters = {
  // Change value of state(s) with getter

};

const mutations = {
  setSourceOptions(state, payload) {
    state.source_options = [];
    payload.data.forEach((instance) => {
      const data = {
        id: instance.id,
        name: instance.name,
      };
      state.source_options.push(data);
    });
  },
  setGroupOptions(state, payload) {
    state.group_options = [];
    payload.data.forEach((instance) => {
      const data = {
        id: instance.id,
        name: instance.name,
      };
      state.group_options.push(data);
    });
  },
  setIndustryOptions(state, payload) {
    state.industry_options = [];
    payload.data.forEach((instance) => {
      const data = {
        id: instance.id,
        name: instance.name,
      };
      state.industry_options.push(data);
    });
  },
  setCountryOptions(state, payload) {
    state.country_options = [];
    payload.data.forEach((instance) => {
      const data = {
        id: instance.id,
        name: instance.name,
      };
      state.country_options.push(data);
    });
  },
  setProvinceOptions(state, payload) {
    state.province_options = [];
    payload.data.forEach((instance) => {
      const data = {
        id: instance.id,
        name: instance.name,
      };
      state.province_options.push(data);
    });
  },
  setCityOptions(state, payload) {
    state.city_options = [];
    payload.data.forEach((instance) => {
      const data = {
        id: instance.id,
        name: instance.name,
      };
      state.city_options.push(data);
    });
  },
  setLeadsDetail(state, payload) {
    state.leads_state.leads_detail = payload.data;
  },
  setLeadsList(state, payload) {
    state.leads_state.leads_list = payload.data;
  },
  setDataChanged(state, param) {
    state.data_changed = param;
  },
};

const actions = {
  async getSourceOptions({ commit, dispatch }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const sourceOptions = await jApiLeads.getSourceOptions();
      commit('setSourceOptions', sourceOptions.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async getGroupOptions({ commit, dispatch }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const groupOptions = await jApiLeads.getGroupOptions();
      commit('setGroupOptions', groupOptions.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async getIndustryOptions({ commit, dispatch }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const industryOptions = await jApiLeads.getIndustryOptions();
      commit('setIndustryOptions', industryOptions.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async getCountryOptions({ commit, dispatch }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const countryOptions = await jApiLeads.getCountryOptions();
      commit('setCountryOptions', countryOptions.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async getProvinceOptions({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const provinceOptions = await jApiLeads.getProvinceOptions(payload);
      commit('setProvinceOptions', provinceOptions.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async getCityOptions({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const cityOptions = await jApiLeads.getCityOptions(payload);
      commit('setCityOptions', cityOptions.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async createLeads({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const resp = await jApiLeads.createLeads(payload);
      commit('setDataChanged', true);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', resp.data.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async getLeadsDetail({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const detailLeads = await jApiLeads.getLeadsDetail(payload);
      commit('setLeadsDetail', detailLeads.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async getListLeads({ commit, dispatch }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const leadsList = await jApiLeads.getListLeads();
      commit('setLeadsList', leadsList.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  setDefaultDataChanged({ commit }) {
    commit('setDataChanged', false);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
