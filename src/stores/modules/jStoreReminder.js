/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import apiReminder from '@/services/jApiReminder';

const state = {
  // Set default state(s) here
  reminder_state: {
    reminder_detail: null,
  },
  data_changed: false,
};

const getters = {

};

const mutations = {
  setReminderDetail(state, payload) {
    state.reminder_state.reminder_detail = payload.data;
  },
  setDataChanged(state, param) {
    state.data_changed = param;
  },
};

const actions = {
  async getReminderDetail({ commit, dispatch }, reqId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const getDetail = await apiReminder.getReminderDetail(reqId);
      commit('setReminderDetail', getDetail.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async createReminder({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const createdReminder = await apiReminder.createReminder(payload);
      commit('setReminderDetail', createdReminder);
      commit('setDataChanged', true);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async updateReminder({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const updatedReminder = await apiReminder.updateReminder(payload);
      commit('setReminderDetail', updatedReminder);
      commit('setDataChanged', true);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async deleteReminder({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      await apiReminder.deleteReminder(payload);
      commit('setDataChanged', true);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  setDefaultDataChanged({ commit }) {
    commit('setDataChanged', false);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
