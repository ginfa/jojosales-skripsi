/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import apiGoal from '@/services/jApiGoals';

const state = {
  // Set default state(s) here
  goal_state: {
    parameter_options: [],
    division_options: null,
    goal_detail: null,
    goal_list: [],
  },
  data_changed: false,
};

const getters = {

};

const mutations = {
  setGoalDetail(state, payload) {
    state.goal_state.goal_detail = payload.data;
  },
  setParameterOptions(state, payload) {
    payload.data.forEach((item) => {
      const option = {
        id: item.id,
        name: item.name,
      };
      state.goal_state.parameter_options.push(option);
    });
  },
  setGoalList(state, payload) {
    state.goal_state.goal_list = payload.data;
  },
  setDataChanged(state, param) {
    state.data_changed = param;
  },
  setDivisionOptions(state, param) {
    if (param !== null) {
      state.goal_state.division_options = [];
      state.goal_state.division_options = param;
    }
  },
};

const actions = {
  async getGoalDetail({ commit, dispatch }, reqId) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const getDetail = await apiGoal.getGoalDetail(reqId);
      commit('setGoalDetail', getDetail);
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async getParameterOptions({ commit, dispatch }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const getActivityTypeList = await apiGoal.getActivityTypeList();
      commit('setParameterOptions', getActivityTypeList.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async createGoal({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const resp = await apiGoal.createGoal(payload);
      commit('setDataChanged', true);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', resp.data.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async updateGoal({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const resp = await apiGoal.updateGoal(payload);
      commit('setDataChanged', true);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', resp.data.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async deleteGoal({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const resp = await apiGoal.deleteGoal(payload);
      commit('setDataChanged', true);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', resp.data.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async getListGoal({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const resp = await apiGoal.getGoalList(payload);
      commit('setGoalList', resp.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  setDefaultDataChanged({ commit }) {
    commit('setDataChanged', false);
  },
  setDefaultDivisionOptions({ commit }, param) {
    commit('setDivisionOptions', param);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
