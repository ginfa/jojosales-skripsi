/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import apiDailyLog from '@/services/jApiDailyLog';

const state = {
  // Set default state(s) here
  my_calendar_state: {
    events: null,
  },
  my_team_state: {
    data: [],
  },
  data_changed: false,
};

const getters = {

};

const mutations = {
  setMyCalendar(state, payload) {
    const storeData = [];
    payload.data.forEach((instance) => {
      const data = {
        start: instance.start_date,
        end: instance.end_date,
        title: instance.opportunity_name,
      };
      storeData.push(data);
      // state.my_calendar_state.events.push(data);
    });
    state.my_calendar_state.events = storeData;
  },
  setMyTeam(state, payload) {
    payload.data.forEach((instance) => {
      const d = new Date(instance.start_date);

      let month = `${d.getMonth() + 1}`;
      let day = `${d.getDate()}`;
      let minutes = `${d.getMinutes()}`;

      if (month.length < 2) month = `0${month}`;
      if (day.length < 2) day = `0${day}`;
      if (minutes.length < 2) minutes = `0${minutes}`;
      const period = (d.getHours() > 12) ? 'PM' : 'AM';
      const hours = (d.getHours() > 12) ? d.getHours() - 12 : d.getHours();

      const date = `${d.getFullYear()}-${month}-${day}`;
      const time = `${hours}:${minutes}${period}`;

      const list = state.my_team_state.data.filter(a => a.date === date)[0];

      const user = {
        id: instance.user_company_id,
        name: `${instance.user.profile.first_name} ${instance.user.profile.last_name}`,
        position: instance.user.position,
        photo: instance.user.photo_url,
      };

      const activity = {
        icon: instance.stage_icon,
        name: instance.opportunity_name,
        time,
        lead: instance.leads_name,
        address: instance.address,
        stage: instance.stage_name,
        color: instance.stage_color,
      };

      if (list.user_list.filter(a => a.id === user.id).length === 0) {
        list.user_list.push(user);
      }
      list.activity_list.push(activity);
    });
  },
  addData(state, param) {
    for (let step = 0; step < 15; step += 1) {
      let month = `${param.getMonth() + 1}`;
      let day = `${param.getDate()}`;
      if (month.length < 2) month = `0${month}`;
      if (day.length < 2) day = `0${day}`;
      const date = `${param.getFullYear()}-${month}-${day}`;

      const data = {
        id: `jojo-${date}`,
        date,
        open: false,
        user_list: [],
        activity_list: [],
      };

      state.my_team_state.data.push(data);

      param = new Date(param);
      param.setDate(param.getDate() + 1);
    }
  },
  openDetail(state, param) {
    const detail = state.my_team_state.data.filter(a => a.id === param.id)[0];
    detail.open = !detail.open;
  },
  setDataChanged(state, param) {
    state.data_changed = param;
  },
};

const actions = {
  async getMyCalendar({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const getEvents = await apiDailyLog.getMyCalendar(payload);
      commit('setMyCalendar', getEvents.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async getMyTeam({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const getList = await apiDailyLog.getMyTeam(payload);
      commit('setMyTeam', getList.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  addData({ commit }, param) {
    commit('addData', param);
  },
  openDetail({ commit }, instance) {
    commit('openDetail', instance);
  },
  setDefaultDataChanged({ commit }) {
    commit('setDataChanged', false);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
