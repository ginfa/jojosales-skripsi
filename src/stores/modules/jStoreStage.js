/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import apiStage from '@/services/jApiStage';
import constant from '../../constant/constant';

const state = {
  // Set default state(s) here
  stage_lg_state: {
    list: [],
  },
  stage_sales_state: {
    list: [],
  },
  activity_type_options: [],
  data_changed: false,
};

const getters = {

};

const mutations = {
  setStageList(state, param) {
    state.stage_lg_state.list = [];
    state.stage_sales_state.list = [];
    param.data.forEach((stage) => {
      stage.is_delete = false;
      if (stage.type === constant.StageTypeLG) {
        const list = [];
        stage.activity_type.forEach((element) => {
          const option = {
            id: element.id,
            name: element.name,
          };
          const options = state.activity_type_options;
          if (options.findIndex(x => x.id === option.id) === -1) {
            options.push(option);
          }
          list.push(element);
        });
        stage.activity_type = list;
        state.stage_lg_state.list.push(stage);
      } else {
        const list = [];
        stage.activity_type.forEach((element) => {
          const option = {
            id: element.id,
            name: element.name,
          };
          const options = state.activity_type_options;
          if (options.findIndex(x => x.id === option.id) === -1) {
            options.push(option);
          }
          list.push(element);
        });
        stage.activity_type = list;
        state.stage_sales_state.list.push(stage);
      }
    });
  },
  setDataChanged(state, param) {
    state.data_changed = param;
  },
};

const actions = {
  async getStageList({ commit, dispatch }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const getStageList = await apiStage.getStageList();
      commit('setStageList', getStageList.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async createStage({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const resp = await apiStage.createStage(payload);
      commit('setDataChanged', true);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', resp.data.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  setDefaultDataChanged({ commit }) {
    commit('setDataChanged', false);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
