/* eslint-disable no-shadow,consistent-return,no-param-reassign */
import jApiIndustry from '@/services/jApiIndustry';

const state = {
  // Set default state(s) here
  industry_state: {
    industry_detail: null,
    industry_list: null,
  },
  data_changed: false,
};

const getters = {
  // Change value of state(s) with getter

};

const mutations = {
  setIndustryDetail(state, payload) {
    state.industry_state.industry_detail = payload.data;
  },
  setIndustryList(state, payload) {
    state.industry_state.industry_list = payload.data;
  },
  setDataChanged(state, param) {
    state.data_changed = param;
  },
};

const actions = {
  async createIndustry({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const resp = await jApiIndustry.createIndustry(payload);
      commit('setDataChanged', true);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', resp.data.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async getIndustryDetail({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const detailIndustry = await jApiIndustry.getIndustryDetail(payload);
      commit('setIndustryDetail', detailIndustry.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async updateIndustry({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const resp = await jApiIndustry.updateIndustry(payload);
      commit('setDataChanged', true);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', resp.data.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async deleteIndustry({ commit, dispatch }, payload) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const resp = await jApiIndustry.deleteIndustry(payload);
      commit('setDataChanged', true);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleSuccess', resp.data.message, { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  async getListIndustry({ commit, dispatch }) {
    dispatch('jStoreNotificationScreen/showLoading', 'Please wait...', { root: true });
    try {
      const industryList = await jApiIndustry.getListIndustry();
      commit('setIndustryList', industryList.data);
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
    } catch (err) {
      dispatch('jStoreNotificationScreen/hideLoading', '', { root: true });
      dispatch('jStoreNotificationScreen/toggleProblem', err.response.data.message, { root: true });
    }
  },
  setDefaultDataChanged({ commit }) {
    commit('setDataChanged', false);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
