import Vue from 'vue';
import Vuex from 'vuex';

// Modules import downhere
import jStoreLogin from './modules/jStoreLogin';
import jStoreNotificationScreen from './modules/jStoreNotificationScreen';
import jStoreHome from './modules/jStoreHome';
import jStoreProduct from './modules/jStoreProduct';
import jStoreGoals from './modules/jStoreGoals';
import jStoreSource from './modules/jStoreSource';
import jStoreMeasure from './modules/jStoreMeasure';
import jStoreReminder from './modules/jStoreReminder';
import jStorePolicy from './modules/jStorePolicy';
import jStoreActivityType from './modules/jStoreActivityType';
import jStoreStage from './modules/jStoreStage';
import jStoreIndustry from './modules/jStoreIndustry';
import jStoreDailyLog from './modules/jStoreDailyLog';
import jStoreReport from './modules/jStoreReport';
import jStorePipeline from './modules/jStorePipeline';
import jStoreLeads from './modules/jStoreLeads';
import jStoreOpportunity from './modules/jStoreOpportunity';

// Initialize vuex
Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
  modules: {
    // Include imported modules in here
    jStoreLogin,
    jStoreNotificationScreen,
    jStoreHome,
    jStoreProduct,
    jStoreSource,
    jStoreMeasure,
    jStoreReminder,
    jStorePolicy,
    jStoreActivityType,
    jStoreGoals,
    jStoreStage,
    jStoreIndustry,
    jStoreDailyLog,
    jStoreReport,
    jStorePipeline,
    jStoreLeads,
    jStoreOpportunity,
  },
  strict: debug,
});
