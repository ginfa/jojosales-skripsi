import jSource from '@/views/config/source/jSource';
import jCreateSource from '@/views/config/source/create-source/jCreateSource';
import jEditSource from '@/views/config/source/edit-source/jEditSource';
import auth from '@/middleware/auth';

export default [
  {
    path: '/config/source',
    name: 'jSource',
    component: jSource,
    meta: {
      title: 'Source Configuration',
      middleware: [auth],
      layout: 'jWrapper',
    },
    children: [
      {
        path: '/config/source/create',
        name: 'jCreateSource',
        component: jCreateSource,
        meta: {
          title: 'Create Source',
          middleware: [auth],
          layout: 'jWrapper',
        },
      },
      {
        path: '/config/source/edit/:idSource',
        name: 'jEditSource',
        component: jEditSource,
        meta: {
          title: 'Edit Source',
          middleware: [auth],
          layout: 'jWrapper',
        },
      },
    ],
  },
];
