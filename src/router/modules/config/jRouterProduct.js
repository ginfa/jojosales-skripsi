import jGlobalProduct from '@/views/config/product/jGlobalProduct';
import jCreateProduct from '@/views/config/product/main/create-product/jCreateProduct';
import jEditProduct from '@/views/config/product/main/edit-product/jEditProduct';
import jCreateMeasure from '@/views/config/product/unit-of-measure/create-measure/jCreateMeasure';
import jEditMeasure from '@/views/config/product/unit-of-measure/edit-measure/jEditMeasure';
import auth from '@/middleware/auth';
import log from '@/middleware/log';

export default [
  {
    path: '/config/product',
    name: 'jGlobalProduct',
    component: jGlobalProduct,
    meta: {
      title: 'Product',
      middleware: [auth, log],
      layout: 'jWrapper',
    },
    children: [
      {
        path: '/config/product/create',
        name: 'jCreateProduct',
        component: jCreateProduct,
        meta: {
          title: 'Create Product',
          middleware: [auth],
        },
      },
      {
        path: '/config/product/edit/:idProduct',
        name: 'jEditProduct',
        component: jEditProduct,
        meta: {
          title: 'Edit Product',
          middleware: [auth],
        },
      },
      {
        path: '/config/product/measure/create',
        name: 'jCreateMeasure',
        component: jCreateMeasure,
        meta: {
          title: 'Config Unit of Measure',
          middleware: [auth],
        },
      },
      {
        path: '/config/product/measure/edit/:idMeasure',
        name: 'jEditMeasure',
        component: jEditMeasure,
        meta: {
          title: 'Edit Unit of Measure',
          middleware: [auth],
        },
      },
    ],
  },
];
