import jIndustry from '@/views/config/industry/jIndustry';
import jCreateIndustry from '@/views/config/industry/create-industry/jCreateIndustry';
import jEditIndustry from '@/views/config/industry/edit-industry/jEditIndustry';
import auth from '@/middleware/auth';

export default [
  {
    path: '/config/industry',
    name: 'jIndustry',
    component: jIndustry,
    meta: {
      title: 'Industry Configuration',
      middleware: [auth],
      layout: 'jWrapper',
    },
    children: [
      {
        path: '/config/industry/create',
        name: 'jCreateIndustry',
        component: jCreateIndustry,
        meta: {
          title: 'Create Industry',
          middleware: [auth],
          layout: 'jWrapper',
        },
      },
      {
        path: '/config/industry/edit/:idIndustry',
        name: 'jEditIndustry',
        component: jEditIndustry,
        meta: {
          title: 'Edit Industry',
          middleware: [auth],
          layout: 'jWrapper',
        },
      },
    ],
  },
];
