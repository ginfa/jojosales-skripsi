import jLogin from '@/views/login/jLogin';

export default [
  {
    path: '/logout',
    name: 'jLogout',
    component: jLogin,
    beforeEnter() {
      localStorage.clear();
      window.location = '/';
    },
  },
];
