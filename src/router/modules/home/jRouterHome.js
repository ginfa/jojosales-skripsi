import jHome from '@/views/home/jHome';
import auth from '@/middleware/auth';
import log from '@/middleware/log';

export default [
  {
    path: '/home',
    name: 'jHome',
    component: jHome,
    meta: {
      title: 'Home',
      middleware: [auth, log],
      layout: 'jWrapper',
    },
  },
];
