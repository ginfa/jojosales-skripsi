import jGlobalDailyLog from '@/views/sales/daily-log/jGlobalDailyLog';
import auth from '@/middleware/auth';

export default [
  {
    path: '/sales/daily-log',
    name: 'jDailyLog',
    component: jGlobalDailyLog,
    meta: {
      title: 'Daily Log',
      middleware: [auth],
      layout: 'jWrapper',
    },
  },
];
