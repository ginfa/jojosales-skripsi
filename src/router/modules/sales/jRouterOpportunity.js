import jGlobalOpportunity from '@/views/sales/opportunity/jGlobalOpportunity';
import jOpportunityDetail from '@/views/sales/opportunity/detail-opportunity/jOpportunityDetail';
import auth from '@/middleware/auth';

export default [
  {
    path: '/sales/opportunity',
    name: 'jOpportunity',
    component: jGlobalOpportunity,
    meta: {
      title: 'Opportunity',
      middleware: [auth],
      layout: 'jWrapper',
    },
    children: [
      {
        path: '/sales/opportunity/detail/:idOpportunity',
        name: 'jDetailOpportunity',
        component: jOpportunityDetail,
        meta: {
          title: 'Detail Opportunity',
          middleware: [auth],
          layout: 'jWrapper',
        },
      },
    ],
  },
];
