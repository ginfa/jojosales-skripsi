import Vue from 'vue';
import Router from 'vue-router';
import jRouterLogin from './modules/login/jRouterLogin';
import jRouterLogout from './modules/logout/jRouterLogout';
import jRouterHome from './modules/home/jRouterHome';
import jRouterProduct from './modules/config/jRouterProduct';
import jRouterGoals from './modules/config/jRouterGoals';
import jRouterSource from './modules/config/jRouterSource';
import jRouterActivityType from './modules/config/jRouterActivityType';
import jRouterStage from './modules/config/jRouterStage';
import jRouterIndustry from './modules/config/jRouterIndustry';
import jRouterDailyLog from './modules/sales/jRouterDailyLog';
import jRouterReport from './modules/config/jRouterReport';
import jRouterPipeline from './modules/sales/jRouterPipeline';
import jRouterLeads from './modules/sales/jRouterLeads';
import jRouterOpportunity from './modules/sales/jRouterOpportunity';

Vue.use(Router);

const moduleRoutes = [
  jRouterLogin,
  jRouterLogout,
  jRouterHome,
  jRouterProduct,
  jRouterSource,
  jRouterActivityType,
  jRouterGoals,
  jRouterStage,
  jRouterIndustry,
  jRouterDailyLog,
  jRouterReport,
  jRouterPipeline,
  jRouterLeads,
  jRouterOpportunity,
];

const baseRoutes = [
  {
    path: '*',
    redirect: '/',
  },
];

const routes = baseRoutes.concat(...moduleRoutes);

const router = new Router({
  mode: 'history',
  routes,
});


// validate route by middleware
function nextFactory(context, middleware, index) {
  const subsequentMiddleware = middleware[index];

  if (!subsequentMiddleware) return context.next;

  return () => {
    const nextMiddleware = nextFactory(context, middleware, index + 1);
    subsequentMiddleware({ ...context, next: nextMiddleware });
  };
}

router.beforeEach((to, from, next) => {
  if (to.meta.middleware) {
    const middleware = Array.isArray(to.meta.middleware)
      ? to.meta.middleware
      : [to.meta.middleware];

    const context = {
      from,
      next,
      router,
      to,
    };

    const nextMiddleware = nextFactory(context, middleware, 1);
    return middleware[0]({ ...context, next: nextMiddleware });
  }
  return next();
});

export default router;
