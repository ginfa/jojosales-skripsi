/* eslint object-shorthand: "error" */
/* eslint-env es6 */

const DEFAULT_IMAGE_URL = 'http://www.hope3k.net/images/placeholders/team-placeholder.jpg';

const SALES_PACKAGE = 9;

// currency
const CURRENCY_IDR = 'IDR';
const CURRENCY_USD = 'USD';
const CURRENCY_EUR = 'EUR';
const CURRENCY_GBP = 'GBP';
const CURRENCY_SGD = 'SGD';

// product type
const PRODUCT_TYPE_SUBSCRIPTION = 1;
const PRODUCT_TYPE_ONE_TIME = 2;

// period type
const PERIOD_TYPE_MONTHS = 1;
const PERIOD_TYPE_DAYS = 2;
const PERIOD_TYPE_HOURS = 3;
const PERIOD_TYPE_MINUTES = 4;
const PERIOD_TYPE_YEARS = 5;
const PERIOD_TYPE_WEEKS = 6;

// period type
const PERIOD_TYPE_MONTHLY = 1;
const PERIOD_TYPE_DAILY = 2;
const PERIOD_TYPE_HOURLY = 3;
const PERIOD_TYPE_MINUTELY = 4;
const PERIOD_TYPE_YEARLY = 5;
const PERIOD_TYPE_WEEKLY = 6;

// reminder type
const REMINDER_TYPE_NOTIFICATION = 1;
const REMINDER_TYPE_EMAIL = 2;

// policy type
const POLICY_REQUIRES_DOCUMENT_TEMPLATE = 1;
const POLICY_REQUIRES_DOCUMENT_UPLOAD = 2;
const POLICY_REQUIRES_PRODUCT = 3;
const POLICY_MAX_NEXT_ACTIVITY = 4;
const POLICY_CLOSE_ACTIVITY = 5;
const POLICY_WINNING_ACTIVITY = 6;

// goal type
const GOAL_TYPE_VALUE = 1;
const GOAL_TYPE_ACTIVITY = 2;

// stage type
const STAGE_TYPE_LG = 1;
const STAGE_TYPE_SALES = 2;

// color
const GREEN = '#41AD49';
const LIGHT_BLUE = '#59B0B2';
const BLUE = '#42A5CE';
const TOSCA = '#0A848E';
const VIOLET = '#3f51b5';
const PURPLE = '#8A0D4F';
const LIGHT_PURPLE = '#9C0D5C';
const RED = '#D2232A';
const ORANGE = '#F36F21';
const YELLOW = '#F8991D';
const LIGHT_YELLOW = '#FFEB3B';
const DARK_BROWN = '#3E2723';
const BROWN = '#4D3921';
const BROWN_GREY = '#938270';
const GREY = '#808285';
const LIGHT_GREY = '#DFDFEA';
const WHITE = '#FFF';

export default {
  DefaultImageUrl: DEFAULT_IMAGE_URL,
  SalesPackage: SALES_PACKAGE,
  CurrencyIDR: CURRENCY_IDR,
  CurrencyUSD: CURRENCY_USD,
  CurrencyEUR: CURRENCY_EUR,
  CurrencyGBP: CURRENCY_GBP,
  CurrencySGD: CURRENCY_SGD,
  ProductTypeSubscription: PRODUCT_TYPE_SUBSCRIPTION,
  ProductTypeOneTime: PRODUCT_TYPE_ONE_TIME,
  PeriodTypeMonths: PERIOD_TYPE_MONTHS,
  PeriodTypeDays: PERIOD_TYPE_DAYS,
  PeriodTypeHours: PERIOD_TYPE_HOURS,
  PeriodTypeMinutes: PERIOD_TYPE_MINUTES,
  PeriodTypeYears: PERIOD_TYPE_YEARS,
  PeriodTypeWeeks: PERIOD_TYPE_WEEKS,
  ReminderTypeNotification: REMINDER_TYPE_NOTIFICATION,
  ReminderTypeEmail: REMINDER_TYPE_EMAIL,
  PolicyRequiresDocumentTemplate: POLICY_REQUIRES_DOCUMENT_TEMPLATE,
  PolicyRequiresDocumentUpload: POLICY_REQUIRES_DOCUMENT_UPLOAD,
  PolicyRequiresProduct: POLICY_REQUIRES_PRODUCT,
  PolicyMaxNextActivity: POLICY_MAX_NEXT_ACTIVITY,
  PolicyCloseActivity: POLICY_CLOSE_ACTIVITY,
  PolicyWinningActivity: POLICY_WINNING_ACTIVITY,
  PeriodTypeMonthly: PERIOD_TYPE_MONTHLY,
  PeriodTypeDaily: PERIOD_TYPE_DAILY,
  PeriodTypeHourly: PERIOD_TYPE_HOURLY,
  PeriodTypeMinutely: PERIOD_TYPE_MINUTELY,
  PeriodTypeYearly: PERIOD_TYPE_YEARLY,
  PeriodTypeWeekly: PERIOD_TYPE_WEEKLY,
  GoalTypeValue: GOAL_TYPE_VALUE,
  GoalTypeNumber: GOAL_TYPE_ACTIVITY,
  StageTypeLG: STAGE_TYPE_LG,
  StageTypeSales: STAGE_TYPE_SALES,
  Green: GREEN,
  LightBlue: LIGHT_BLUE,
  Blue: BLUE,
  Tosca: TOSCA,
  Violet: VIOLET,
  Purple: PURPLE,
  LightPurple: LIGHT_PURPLE,
  Red: RED,
  Orange: ORANGE,
  Yellow: YELLOW,
  LightYellow: LIGHT_YELLOW,
  DarkBrown: DARK_BROWN,
  Brown: BROWN,
  BrownGrey: BROWN_GREY,
  Grey: GREY,
  LightGrey: LIGHT_GREY,
  White: WHITE,
  configureColor(val) {
    if (val === this.Green) {
      return 'green';
    }
    if (val === this.LightBlue) {
      return 'light-blue';
    }
    if (val === this.Blue) {
      return 'blue';
    }
    if (val === this.Tosca) {
      return 'tosca';
    }
    if (val === this.Violet) {
      return 'violet';
    }
    if (val === this.Purple) {
      return 'purple';
    }
    if (val === this.LightPurple) {
      return 'light-purple';
    }
    if (val === this.Red) {
      return 'red';
    }
    if (val === this.Orange) {
      return 'orange';
    }
    if (val === this.Yellow) {
      return 'yellow';
    }
    if (val === this.LightYellow) {
      return 'light-yellow';
    }
    if (val === this.DarkBrown) {
      return 'dark-brown';
    }
    if (val === this.Brown) {
      return 'brown';
    }
    if (val === this.BrownGrey) {
      return 'brownish-grey';
    }
    if (val === this.Grey) {
      return 'grey';
    }
    if (val === this.LightGrey) {
      return 'light-grey';
    }
    return '';
  },
};
