import jApi from './jApi';

export default {
  getMeasureDetail(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/unit-of-measure/detail', req)
      .then(res => res.data);
  },

  getMeasureDataTable(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/unit-of-measure/data-table', req)
      .then(res => res);
  },

  createMeasure(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/unit-of-measure/create', req)
      .then(res => res);
  },

  updateMeasure(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/unit-of-measure/update', req)
      .then(res => res);
  },

  deleteMeasure(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/unit-of-measure/delete', req)
      .then(res => res);
  },

  getMeasureList() {
    const api = jApi.generateApi();
    return api.get('sales/setup/unit-of-measure/list')
      .then(res => res);
  },
};
