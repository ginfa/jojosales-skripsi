import jApi from './jApi';

export default {
  getSourceDetail(req) {
    const api = jApi.generateApi();
    return api.post('sales/leads/source/detail', req)
      .then(res => res);
  },

  createSource(req) {
    const api = jApi.generateApi();
    return api.post('sales/leads/source/create', req)
      .then(res => res);
  },

  updateSource(req) {
    const api = jApi.generateApi();
    return api.post('sales/leads/source/update', req)
      .then(res => res);
  },

  deleteSource(req) {
    const api = jApi.generateApi();
    return api.post('sales/leads/source/delete', req)
      .then(res => res);
  },

  getListSource(req) {
    const api = jApi.generateApi();
    return api.get('sales/leads/source/list', req)
      .then(res => res);
  },
};
