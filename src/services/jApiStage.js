import jApi from './jApi';

export default {
  getStageList() {
    const api = jApi.generateApi();
    return api.get('sales/setup/stage/list')
      .then(res => res);
  },

  createStage(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/stage/create', req)
      .then(res => res);
  },

};
