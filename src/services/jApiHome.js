import jApi from './jApi';

export default {
  getOpportunityGraph() {
    const api = jApi.generateApi();
    return api.get('sales/report/graph/opportunity')
      .then(res => res);
  },

  getWinLostGraph(req) {
    const api = jApi.generateApi();
    return api.post('sales/report/graph/deal-and-lost', req)
      .then(res => res);
  },

  getActivityDetail(req) {
    const api = jApi.generateApi();
    return api.post('sales/activity/my-calendar', req)
      .then(res => res);
  },

  getLeaderboard(req) {
    const api = jApi.generateApi();
    return api.post('sales/leaderboard', req)
      .then(res => res);
  },
};
