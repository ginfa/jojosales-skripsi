import jApi from './jApi';

export default {
  getActivityTypeList(req) {
    const api = jApi.generateApi();
    return api.get('sales/setup/activity-type/list', req)
      .then(res => res);
  },

  getGoalDetail(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/goal/detail', req)
      .then(res => res.data);
  },

  getGoalList(req) {
    const api = jApi.generateApi();
    return api.get('sales/setup/goal/list', req)
      .then(res => res);
  },

  createGoal(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/goal/create', req)
      .then(res => res);
  },

  updateGoal(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/goal/update', req)
      .then(res => res);
  },

  deleteGoal(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/goal/delete', req)
      .then(res => res);
  },
};
