import jApi from './jApi';

export default {
  getActivityTypeDetail(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/activity-type/detail', req)
      .then(res => res.data);
  },

  getActivityTypeDataTable(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/activity-type/data-table', req)
      .then(res => res);
  },

  createActivityType(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/activity-type/create', req)
      .then(res => res);
  },

  updateActivityType(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/activity-type/update', req)
      .then(res => res);
  },

  deleteActivityType(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/activity-type/delete', req)
      .then(res => res);
  },

  getActivityTypeList() {
    const api = jApi.generateApi();
    return api.get('sales/setup/activity-type/list')
      .then(res => res);
  },
};
