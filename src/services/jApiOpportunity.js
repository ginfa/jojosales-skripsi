import jApi from './jApi';

export default {
  getOpportunityDataTable(req) {
    const api = jApi.generateApi();
    return api.post('sales/opportunity/data-table', req)
      .then(res => res);
  },

  getOpportunityDetail(req) {
    const api = jApi.generateApi();
    return api.post('sales/opportunity/detail', req)
      .then(res => res);
  },

  getOpportunityByLead(req) {
    const api = jApi.generateApi();
    return api.post('sales/opportunity/by-leads', req)
      .then(res => res);
  },
};
