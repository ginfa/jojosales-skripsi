import jApi from './jApi';

export default {
  getSourceOptions() {
    const api = jApi.generateApi();
    return api.get('sales/leads/source/list')
      .then(res => res);
  },

  getIndustryOptions() {
    const api = jApi.generateApi();
    return api.get('sales/setup/industry/list')
      .then(res => res);
  },

  getGroupOptions() {
    const api = jApi.generateApi();
    return api.get('sales/leads/group/list')
      .then(res => res);
  },

  getCountryOptions() {
    const api = jApi.generateApi();
    return api.get('company/tools/countries')
      .then(res => res);
  },

  getProvinceOptions(req) {
    const api = jApi.generateApi();
    return api.post('company/tools/provinces', req)
      .then(res => res);
  },

  getCityOptions(req) {
    const api = jApi.generateApi();
    return api.post('company/tools/cities', req)
      .then(res => res);
  },

  getLeadsDetail(req) {
    const api = jApi.generateApi();
    return api.post('sales/leads/detail', req)
      .then(res => res);
  },

  createLeads(req) {
    const api = jApi.generateApi();
    return api.post('sales/leads/create', req)
      .then(res => res);
  },

  updateLeads(req) {
    const api = jApi.generateApi();
    return api.post('sales/leads/update', req)
      .then(res => res);
  },

  getLeadsDataTable(req) {
    const api = jApi.generateApi();
    return api.post('sales/leads/data-table', req)
      .then(res => res);
  },
};
