import jApi from './jApi';

export default {
  getIndustryDetail(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/industry/detail', req)
      .then(res => res);
  },

  createIndustry(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/industry/create', req)
      .then(res => res);
  },

  updateIndustry(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/industry/update', req)
      .then(res => res);
  },

  deleteIndustry(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/industry/delete', req)
      .then(res => res);
  },

  getListIndustry(req) {
    const api = jApi.generateApi();
    return api.get('sales/setup/industry/list', req)
      .then(res => res);
  },
};
