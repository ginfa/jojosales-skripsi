import jApi from './jApi';

export default {
  getReminderDataTable(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/activity-type-reminder/data-table', req)
      .then(res => res);
  },

  createReminder(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/activity-type-reminder/create', req)
      .then(res => res);
  },

  updateReminder(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/activity-type-reminder/update', req)
      .then(res => res);
  },

  deleteReminder(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/activity-type-reminder/delete', req)
      .then(res => res);
  },

  getReminderDetail(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/activity-type-reminder/detail', req)
      .then(res => res);
  },

  getReminderList() {
    const api = jApi.generateApi();
    return api.get('sales/setup/activity-type-reminder/list')
      .then(res => res);
  },
};
