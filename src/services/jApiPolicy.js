import jApi from './jApi';

export default {
  getPolicyDetail(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/activity-type-policy/detail', req)
      .then(res => res.data);
  },

  getPolicyDataTable(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/activity-type-policy/data-table', req)
      .then(res => res);
  },

  createPolicy(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/activity-type-policy/create', req)
      .then(res => res);
  },

  updatePolicy(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/activity-type-policy/update', req)
      .then(res => res);
  },

  deletePolicy(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/activity-type-policy/delete', req)
      .then(res => res);
  },

  getPolicyList() {
    const api = jApi.generateApi();
    return api.get('sales/setup/activity-type-policy/list')
      .then(res => res);
  },
};
