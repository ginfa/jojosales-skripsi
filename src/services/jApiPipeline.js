import jApi from './jApi';

export default {
  getPipeline(req) {
    const api = jApi.generateApi();
    return api.post('sales/pipeline', req)
      .then(res => res);
  },
};
