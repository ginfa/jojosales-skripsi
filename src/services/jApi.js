import axios from 'axios';
import Vue from 'vue';
// eslint-disable-next-line
import stores from '@/stores';
// eslint-disable-next-line
import router from '@/router';

export default {
  generateApi() {
    const token = Vue.ls.get('Token');

    const api = axios.create({
      baseURL: process.env.GATE_URL,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    api.interceptors.response.use(
      (response) => {
        if (typeof response.headers.token !== 'undefined') {
          stores.commit('jStoreLogin/saveToken', response.headers);
          stores.commit('jStoreLogin/setInLocalStorage');
        }
        return response;
      },
      (error) => {
        if (typeof error.response.data.session_expired === 'boolean') {
          if (error.response.data.session_expired === true) {
            router.push('/logout');
          }
        } else if (error.response.status === 406 || error.response.status === 401) {
          router.push('/logout');
        }
        // handle error
        return Promise.reject(error);
      });

    return api;
  },
  loginApi() {
    const api = axios.create({
      baseURL: process.env.GATE_URL,
    });
    return api;
  },
};
