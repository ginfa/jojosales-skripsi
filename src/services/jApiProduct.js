import jApi from './jApi';

export default {
  getProductDetail(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/product/detail', req)
      .then(res => res.data);
  },

  getProductDataTable(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/product/data-table', req)
      .then(res => res);
  },

  createProduct(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/product/create', req)
      .then(res => res);
  },

  updateProduct(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/product/update', req)
      .then(res => res);
  },

  deleteProduct(req) {
    const api = jApi.generateApi();
    return api.post('sales/setup/product/delete', req)
      .then(res => res);
  },
};
