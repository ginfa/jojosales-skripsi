/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions, mapState } from 'vuex';
import jApi from '@/services/jApi';
import apiMeasure from '@/services/jApiMeasure';
import jReusableFunction from '@/mixins/paginations/jReusableFunction';

export const jMixPaginationMeasure = {
  data() {
    return {
      filter_measure: '',
      data_table: [],
      selected_rows: [],
      load_first: false,
      columns: ['checkbox', 'name', 'description'],
      options: {
        headings: {
          name: 'NAME',
          description: 'DESCRIPTION',
          checkbox(h) {
            const objThis = this;
            return jReusableFunction.checkbox(h, objThis);
          },
        },
        texts: {
          filterPlaceholder: 'Search',
          limit: 'Rows per page',
        },
        sortIcon: {
          base: 'fa',
          up: 'fa-chevron-up',
          down: 'fa-chevron-down',
          is: 'fa-sort',
        },
        debounce: 1000,
        filterable: true,
        sortable: ['name'],
        perPage: 5,
        perPageValues: [5, 10, 20, 50, 100],
        requestAdapter(data) {
          return {
            pagination: {
              limit: data.limit,
              page: data.page,
              column: data.orderBy ? data.orderBy : 'id',
              ascending: !!data.ascending,
              query: data.query ? data.query : '',
            },
          };
        },
        requestFunction(data) {
          const getListProduct = apiMeasure.getMeasureDataTable(data);
          return getListProduct.catch((e) => {
            this.$store.dispatch('jStoreNotificationScreen/toggleProblem', e.response.data.errors[0].message).bind(this);
          });
        },
        responseAdapter({ data }) {
          return {
            data: data.data ? data.data : [],
            count: data.length ? data.length : 0,
          };
        },
      },
    };
  },
  computed: {
    ...mapState('jStoreMeasure', ['data_changed']),
  },
  methods: {
    ...mapActions('jStoreNotificationScreen', ['toggleProblem', 'showLoading', 'hideLoading']),
    ...mapActions('jStoreMeasure', ['setDefaultDataChanged']),

    loading() {
      if (this.load_first) {
        this.showLoading();
      }
    },

    loaded(data) {
      this.data_table = data.data.data;
      if (this.load_first) {
        this.hideLoading();
      } else {
        this.load_first = !this.load_first;
      }
    },

    refreshTable() {
      this.$refs.measureServer.getData();
    },

    measuresDeleted() {
      this.refreshTable();
      this.selected_rows = [];
    },

    customFilter(val) {
      if (val !== '') {
        this.$refs.measureServer.setFilter(val);
      }
    },
  },
  watch: {
    selected_rows() {
      if (this.selected_rows.length === 0) {
        this.$refs.floating.hide();
      } else {
        this.$refs.floating.show();
      }
    },
    filter_measure(val) {
      if (val === '') {
        this.$refs.measureServer.setFilter('');
      }
    },
    data_changed(val) {
      if (val) {
        this.$router.replace('/config/product');
        this.refreshTable();
        this.setDefaultDataChanged();
      }
    },
  },
};
