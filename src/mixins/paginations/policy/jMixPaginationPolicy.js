/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions, mapState } from 'vuex';
import apiPolicy from '@/services/jApiPolicy';
import jReusableFunction from '@/mixins/paginations/jReusableFunction';

export const jMixPaginationPolicy = {
  data() {
    return {
      filter_policy: '',
      data_table: [],
      selected_rows: [],
      load_first: false,
      columns: ['checkbox', 'name', 'description', 'win', 'close', 'product', 'template'],
      options: {
        headings: {
          name: 'NAME',
          description: 'DESCRIPTION',
          win: 'WIN',
          close: 'CLOSE OPPORTUNITY',
          product: 'REQUIRES PRODUCT',
          template: 'DOCUMENT TEMPLATE',
          checkbox(h) {
            const objThis = this;
            return jReusableFunction.checkbox(h, objThis);
          },
        },
        texts: {
          filterPlaceholder: 'Search',
          limit: 'Rows per page',
        },
        sortIcon: {
          base: 'fa',
          up: 'fa-chevron-up',
          down: 'fa-chevron-down',
          is: 'fa-sort',
        },
        debounce: 1000,
        filterable: true,
        sortable: ['name'],
        perPage: 5,
        perPageValues: [5, 10, 20, 50, 100],
        requestAdapter(data) {
          return {
            pagination: {
              limit: data.limit,
              page: data.page,
              column: data.orderBy ? data.orderBy : 'id',
              ascending: !!data.ascending,
              query: data.query ? data.query : '',
            },
          };
        },
        requestFunction(data) {
          const getListProduct = apiPolicy.getPolicyDataTable(data);
          return getListProduct.catch((e) => {
            this.$store.dispatch('jStoreNotificationScreen/toggleProblem', e.response.data.errors[0].message).bind(this);
          });
        },
        responseAdapter({ data }) {
          return {
            data: data.data ? data.data : [],
            count: data.length ? data.length : 0,
          };
        },
      },
    };
  },
  computed: {
    ...mapState('jStorePolicy', ['data_changed']),
  },
  methods: {
    ...mapActions('jStoreNotificationScreen', ['toggleProblem', 'showLoading', 'hideLoading']),
    ...mapActions('jStorePolicy', ['setDefaultDataChanged']),

    loading() {
      if (this.load_first) {
        this.showLoading();
      }
    },

    loaded(data) {
      this.data_table = data.data.data;
      if (this.load_first) {
        this.hideLoading();
      } else {
        this.load_first = !this.load_first;
      }
    },

    refreshTable() {
      this.$refs.policyServer.getData();
    },

    policysDeleted() {
      this.refreshTable();
      this.selected_rows = [];
    },

    customFilter(val) {
      if (val !== '') {
        this.$refs.policyServer.setFilter(val);
      }
    },
  },
  watch: {
    selected_rows() {
      if (this.selected_rows.length === 0) {
        this.$refs.floating.hide();
      } else {
        this.$refs.floating.show();
      }
    },
    filter_policy(val) {
      if (val === '') {
        this.$refs.policyServer.setFilter('');
      }
    },
    data_changed(val) {
      if (val === true) {
        this.refreshTable();
        this.setDefaultDataChanged();
        this.$router.replace('/config/activity-type');
      }
    },
  },
};
