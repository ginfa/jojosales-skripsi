/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing,no-param-reassign */
import { mapActions, mapState } from 'vuex';
import apiOpportunity from '@/services/jApiOpportunity';

export const jMixPaginationOpportunity = {
  data() {
    return {
      filter_opportunity: '',
      filter_type: 'opportunity_name',
      filter_options: [
        {
          value: 'opportunity_name',
          text: 'Name',
        },
        {
          value: 'leads_name',
          text: 'Leads',
        },
        {
          value: 'sales_in_charge',
          text: 'Sales in Charge',
        },
      ],
      data_table: [],
      load_first: false,
      columns: ['add_detail', 'opportunity', 'company', 'sales', 'product_name', 'product_duration', 'product_total_amount', 'status', 'won'],
      options: {
        headings: {
          add_detail: 'MORE DETAILS',
          opportunity: 'OPPORTUNITY',
          company: 'COMPANY',
          sales: 'SALES IN CHARGE',
          product_name: 'PRODUCT',
          product_duration: 'DURATION',
          product_total_amount: 'TOTAL AMOUNT',
          status: 'STATUS',
          won: 'WON',
        },
        texts: {
          filterPlaceholder: 'Search',
          limit: 'Rows per page',
        },
        sortIcon: {
          base: 'fa',
          up: 'fa-chevron-up',
          down: 'fa-chevron-down',
          is: 'fa-sort',
        },
        debounce: 1000,
        filterable: true,
        sortable: ['name'],
        perPage: 5,
        perPageValues: [5, 10, 20, 50, 100],
        requestAdapter(data) {
          return {
            pagination: {
              limit: data.limit,
              page: data.page,
              column: data.orderBy ? data.orderBy : 'id',
              ascending: !!data.ascending,
              query: data.query ? data.query : '',
            },
          };
        },
        requestFunction(data) {
          const getListProduct = apiOpportunity.getOpportunityDataTable(data);
          return getListProduct.catch((e) => {
            this.$store.dispatch('jStoreNotificationScreen/toggleProblem', e.response.data.errors[0].message).bind(this);
          });
        },
        responseAdapter({ data }) {
          data.data.forEach((instance) => {
            instance.add_detail = false;
            if (typeof instance.activity !== 'undefined') {
              instance.show_toggle_detail = (instance.activity.product.length > 1);
              instance.product_name = [instance.activity.product[0].name];
              instance.product_duration = [instance.activity.product[0].duration];
              instance.product_total_amount = [instance.activity.product[0].price
              * instance.activity.product[0].total_unit_of_measure];
            } else {
              instance.product_name = ['-'];
              instance.product_duration = ['-'];
              instance.product_total_amount = ['-'];
            }
          });
          return {
            data: data.data ? data.data : 0,
            count: data.length ? data.length : 0,
          };
        },
      },
    };
  },
  computed: {
    ...mapState('jStoreOpportunity', ['data_changed']),
  },
  methods: {
    ...mapActions('jStoreNotificationScreen', ['toggleProblem', 'showLoading', 'hideLoading']),
    ...mapActions('jStoreOpportunity', ['setDefaultDataChanged']),

    loading(val) {
      // eslint-disable-next-line
      val.pagination.query_type = this.filter_type;
      if (this.load_first) {
        this.showLoading();
      }
    },

    loaded(data) {
      this.data_table = data.data.data;
      if (this.load_first) {
        this.hideLoading();
      } else {
        this.load_first = !this.load_first;
      }
    },

    refreshTable() {
      this.$refs.opportunityServer.getData();
    },

    customFilter(val) {
      if (val !== '') {
        this.$refs.opportunityServer.setFilter(val);
      }
    },
  },
  watch: {
    selected_rows() {
      if (this.selected_rows.length === 0) {
        this.$refs.floating.hide();
      } else {
        this.$refs.floating.show();
      }
    },
    filter_opportunity(val) {
      if (val === '') {
        this.$refs.opportunityServer.setFilter('');
      }
    },
    data_changed(val) {
      if (val) {
        this.$router.replace('/sales/opportunity');
        this.refreshTable();
        this.setDefaultDataChanged();
      }
    },
  },
};

export const jMixPaginationOpportunityByLeads = {
  data() {
    return {
      data_table: [],
      load_first: false,
      columns: ['add_detail', 'opportunity', 'sales', 'pic_list', 'product_list', 'status'],
      options: {
        headings: {
          add_detail: 'MORE DETAILS',
          opportunity: 'OPPORTUNITY',
          sales: 'SALES IN CHARGE',
          pic_list: 'CONTACT',
          product_list: 'PRODUCT',
          status: 'STATUS',
        },
        sortIcon: {
          base: 'fa',
          up: 'fa-chevron-up',
          down: 'fa-chevron-down',
          is: 'fa-sort',
        },
        pagination: { chunk: 10 },
        debounce: 1000,
        filterable: false,
        sortable: ['opportunity'],
      },
    };
  },
  computed: {
    ...mapState('jStoreOpportunity', ['data_changed', 'opportunity_state']),
  },
  beforeMount() {
    this.getDataTable();
  },
  methods: {
    ...mapActions('jStoreOpportunity', ['setDefaultDataChanged', 'getOpportunityByLead']),
    getDataTable() {
      const leadsId = {
        leads_id: Number(this.$route.params.idLeads),
      };

      this.getOpportunityByLead(leadsId);
    },
  },
  watch: {
    'opportunity_state.opportunity_list'(val) {
      this.load_first = !this.load_first;
      this.data_table = val;
    },
    data_changed(val) {
      if (val) {
        this.$router.replace('/sales/opportunity');
        this.setDefaultDataChanged();
      }
    },
  },
};
