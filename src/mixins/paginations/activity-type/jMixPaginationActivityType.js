/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions, mapState } from 'vuex';
import apiActivityType from '@/services/jApiActivityType';
import jReusableFunction from '@/mixins/paginations/jReusableFunction';

export const jMixPaginationActivityType = {
  data() {
    return {
      filter_activity_type: '',
      data_table: [],
      selected_rows: [],
      load_first: false,
      columns: ['checkbox', 'name', 'description', 'date', 'policy', 'reminder'],
      options: {
        headings: {
          name: 'NAME',
          description: 'DESCRIPTION',
          date: 'CREATED DATE -- UPDATED DATE',
          policy: 'POLICY',
          reminder: 'REMINDER',
          checkbox(h) {
            const objThis = this;
            return jReusableFunction.checkbox(h, objThis);
          },
        },
        texts: {
          filterPlaceholder: 'Search',
          limit: 'Rows per page',
        },
        sortIcon: {
          base: 'fa',
          up: 'fa-chevron-up',
          down: 'fa-chevron-down',
          is: 'fa-sort',
        },
        debounce: 1000,
        filterable: true,
        sortable: ['name'],
        perPage: 5,
        perPageValues: [5, 10, 20, 50, 100],
        requestAdapter(data) {
          return {
            pagination: {
              limit: data.limit,
              page: data.page,
              column: data.orderBy ? data.orderBy : 'id',
              ascending: !!data.ascending,
              query: data.query ? data.query : '',
            },
          };
        },
        requestFunction(data) {
          const getListActivityType = apiActivityType.getActivityTypeDataTable(data);
          return getListActivityType.catch((e) => {
            this.$store.dispatch('jStoreNotificationScreen/toggleProblem', e.response.data.errors[0].message).bind(this);
          });
        },
        responseAdapter({ data }) {
          return {
            data: data.data ? data.data : [],
            count: data.length ? data.length : 0,
          };
        },
      },
    };
  },
  computed: {
    ...mapState('jStoreActivityType', ['data_changed']),
  },
  methods: {
    ...mapActions('jStoreNotificationScreen', ['toggleProblem', 'showLoading', 'hideLoading']),
    ...mapActions('jStoreActivityType', ['setDefaultDataChanged']),

    loading() {
      if (this.load_first) {
        this.showLoading();
      }
    },

    loaded(data) {
      this.data_table = data.data.data;
      if (this.load_first) {
        this.hideLoading();
      } else {
        this.load_first = !this.load_first;
      }
    },

    refreshTable() {
      this.$refs.activityTypeServer.getData();
    },

    activityTypesDeleted() {
      this.refreshTable();
      this.selected_rows = [];
    },

    customFilter(val) {
      if (val !== '') {
        this.$refs.activityTypeServer.setFilter(val);
      }
    },
  },
  watch: {
    selected_rows() {
      if (this.selected_rows.length === 0) {
        this.$refs.floating.hide();
      } else {
        this.$refs.floating.show();
      }
    },
    filter_activity_type(val) {
      if (val === '') {
        this.$refs.activityTypeServer.setFilter('');
      }
    },
    data_changed(val) {
      if (val) {
        this.$router.replace('/config/activity-type');
        this.refreshTable();
        this.setDefaultDataChanged();
      }
    },
  },
};
