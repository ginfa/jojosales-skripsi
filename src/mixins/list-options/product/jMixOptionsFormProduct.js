/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions, mapState } from 'vuex';
import constant from '../../../constant/constant';

export const jMixOptionsFormProduct = {
  created() {
    this.optionsGeneral();
  },

  computed: {
    ...mapState('jStoreProduct', ['product_state']),
    currency_options() {
      return [
        {
          name: 'IDR',
          id: constant.CurrencyIDR,
        },
        // {
        //   text: 'USD',
        //   value: constant.CurrencyUSD,
        // },
        // {
        //   text: 'EUR',
        //   value: constant.CurrencyEUR,
        // },
        // {
        //   text: 'GBP',
        //   value: constant.CurrencyGBP,
        // },
        // {
        //   text: 'SGD',
        //   value: constant.CurrencySGD,
        // },
      ];
    },
    product_type_options() {
      return [
        {
          name: 'Subscription',
          id: constant.ProductTypeSubscription,
        },
        {
          name: 'One Time',
          id: constant.ProductTypeOneTime,
        },
      ];
    },
    period_type_options() {
      return [
        {
          name: 'Hourly',
          id: constant.PeriodTypeHourly,
        },
        {
          name: 'Daily',
          id: constant.PeriodTypeDaily,
        },
        {
          name: 'Weekly',
          id: constant.PeriodTypeWeekly,
        },
        {
          name: 'Monthly',
          id: constant.PeriodTypeMonthly,
        },
        {
          name: 'Yearly',
          id: constant.PeriodTypeYearly,
        },
      ];
    },
  },
  methods: {
    ...mapActions('jStoreProduct', ['getMeasureOptions']),
    optionsGeneral() {
      // options which use in mode create and edit.
      this.getMeasureOptions();
    },
  },

};
