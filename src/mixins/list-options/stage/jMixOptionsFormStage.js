/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing,no-param-reassign */
/* eslint-disable import/no-extraneous-dependencies */
import { ASYNC_SEARCH } from '@riophae/vue-treeselect';
import jApi from '@/services/jApi';
import constant from '../../../constant/constant';

export const jMixOptionsFormStage = {
  computed: {
    color_options() {
      return [
        {
          name: 'Green',
          id: constant.Green,
        },
        {
          name: 'Light Blue',
          id: constant.LightBlue,
        },
        {
          name: 'Blue',
          id: constant.Blue,
        },
        {
          name: 'Tosca',
          id: constant.Tosca,
        },
        {
          name: 'Violet',
          id: constant.Violet,
        },
        {
          name: 'Purple',
          id: constant.Purple,
        },
        {
          name: 'Light Purple',
          id: constant.LightPurple,
        },
        {
          name: 'Red',
          id: constant.Red,
        },
        {
          name: 'Orange',
          id: constant.Orange,
        },
        {
          name: 'Yellow',
          id: constant.Yellow,
        },
        {
          name: 'Light Yellow',
          id: constant.LightYellow,
        },
        {
          name: 'Dark Brown',
          id: constant.DarkBrown,
        },
        {
          name: 'Brown',
          id: constant.Brown,
        },
        {
          name: 'Brownish Grey',
          id: constant.BrownGrey,
        },
        {
          name: 'Grey',
          id: constant.Grey,
        },
        {
          name: 'Light Grey',
          id: constant.LightGrey,
        },
      ];
    },
    icon_options() {
      return [
        {
          id: 'jojosales_icons',
          name: 'Icon',
        },
        {
          id: 'jojosales_icons2',
          name: 'Icon2',
        },
        {
          id: 'jojosales_icons3',
          name: 'Icon3',
        },
        {
          id: 'jojosales_icons4',
          name: 'Icon4',
        },
        {
          id: 'jojosales_icons5',
          name: 'Icon5',
        },
        {
          id: 'jojosales_icons6',
          name: 'Icon6',
        },
        {
          id: 'jojosales_icons7',
          name: 'Icon7',
        },
        {
          id: 'jojosales_icons8',
          name: 'Icon8',
        },
        {
          id: 'jojosales_icons9',
          name: 'Icon9',
        },
        {
          id: 'jojosales_icons10',
          name: 'Icon10',
        },
        {
          id: 'jojosales_icons11',
          name: 'Icon11',
        },
        {
          id: 'jojosales_icons12',
          name: 'Icon12',
        },
        {
          id: 'jojosales_icons13',
          name: 'Icon13',
        },
        {
          id: 'jojosales_icons14',
          name: 'Icon14',
        },
        {
          id: 'jojosales_icons15',
          name: 'Icon15',
        },
        {
          id: 'jojosales_icons16',
          name: 'Icon16',
        },
        {
          id: 'jojosales_icons17',
          name: 'Icon17',
        },
        {
          id: 'jojosales_icons18',
          name: 'Icon18',
        },
        {
          id: 'jojosales_icons19',
          name: 'Icon19',
        },
        {
          id: 'jojosales_icons20',
          name: 'Icon20',
        },
        {
          id: 'jojosales_icons21',
          name: 'Icon21',
        },
        {
          id: 'jojosales_icons22',
          name: 'Icon22',
        },
        {
          id: 'jojosales_icons23',
          name: 'Icon23',
        },
      ];
    },
  },
  methods: {
    loadActivityTypeOptions({ action, searchQuery, callback }) {
      if (action === ASYNC_SEARCH) {
        const reqData = {
          pagination: {
            limit: 0,
            page: 0,
            column: 'name',
            ascending: true,
            query: searchQuery,
          },
        };
        this.getListDivision(reqData, callback);
      }
    },
    getListDivision(req, callback) {
      const api = jApi.generateApi();
      return api.post('sales/setup/activity-type/data-table', req)
        .then((res) => {
          if (typeof res.data.data !== 'undefined') {
            res.data.data.forEach((lab, index) => {
              res.data.data[index].label = lab.name;
            });
            callback(null, res.data.data);
          }
          return res.data;
        });
    },
  },

};
