/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */

export const jMixOptionsFormSource = {
  created() {
    this.optionsGeneral();
    if (this.mode === 'create') {
      this.optionsCreate();
    } else if (this.mode === 'edit') {
      this.optionsEdit();
    }
  },

  computed: {
    icon_options() {
      return [
        {
          id: 'jojosales_icons',
          name: 'Icon',
        },
        {
          id: 'jojosales_icons2',
          name: 'Icon2',
        },
        {
          id: 'jojosales_icons3',
          name: 'Icon3',
        },
        {
          id: 'jojosales_icons4',
          name: 'Icon4',
        },
        {
          id: 'jojosales_icons5',
          name: 'Icon5',
        },
        {
          id: 'jojosales_icons6',
          name: 'Icon6',
        },
        {
          id: 'jojosales_icons7',
          name: 'Icon7',
        },
        {
          id: 'jojosales_icons8',
          name: 'Icon8',
        },
        {
          id: 'jojosales_icons9',
          name: 'Icon9',
        },
        {
          id: 'jojosales_icons10',
          name: 'Icon10',
        },
        {
          id: 'jojosales_icons11',
          name: 'Icon11',
        },
        {
          id: 'jojosales_icons12',
          name: 'Icon12',
        },
        {
          id: 'jojosales_icons13',
          name: 'Icon13',
        },
        {
          id: 'jojosales_icons14',
          name: 'Icon14',
        },
        {
          id: 'jojosales_icons15',
          name: 'Icon15',
        },
        {
          id: 'jojosales_icons16',
          name: 'Icon16',
        },
        {
          id: 'jojosales_icons17',
          name: 'Icon17',
        },
        {
          id: 'jojosales_icons18',
          name: 'Icon18',
        },
        {
          id: 'jojosales_icons19',
          name: 'Icon19',
        },
        {
          id: 'jojosales_icons20',
          name: 'Icon20',
        },
        {
          id: 'jojosales_icons21',
          name: 'Icon21',
        },
        {
          id: 'jojosales_icons22',
          name: 'Icon22',
        },
        {
          id: 'jojosales_icons23',
          name: 'Icon23',
        },
      ];
    },
  },

  methods: {
    optionsGeneral() {
      // options which use in mode create and edit.
    },

    optionsCreate() {
      // options which use in mode create only.
    },

    optionsEdit() {
      // options which use in mode edit only.
    },
  },

};
