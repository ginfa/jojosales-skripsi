import { mapActions, mapState } from 'vuex';

/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */

export const jMixOptionsFormLeads = {
  created() {
    this.optionsGeneral();
  },

  computed: {
    ...mapState('jStoreLeads', ['source_options', 'industry_options', 'group_options', 'city_options', 'province_options', 'country_options']),
  },

  watch: {
    'leads_data.country_id'(val) {
      if (val > 0) {
        this.getProvinceOptions({ country_id: val });
      }
    },
    'leads_data.province_id'(val) {
      if (val > 0) {
        this.getCityOptions({ province_id: val });
      }
    },
  },

  methods: {
    ...mapActions('jStoreLeads', ['getSourceOptions', 'getGroupOptions', 'getIndustryOptions', 'getCountryOptions', 'getProvinceOptions', 'getCityOptions']),
    optionsGeneral() {
      this.getSourceOptions();
      this.getGroupOptions();
      this.getIndustryOptions();
      this.getCountryOptions();
    },
  },

};
