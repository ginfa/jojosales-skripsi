/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */

export const jMixOptionsFormMeasure = {
  created() {
    this.optionsGeneral();
    if (this.mode === 'create') {
      this.optionsCreate();
    } else if (this.mode === 'edit') {
      this.optionsEdit();
    }
  },

  methods: {
    optionsGeneral() {
      // options which use in mode create and edit.
    },

    optionsCreate() {
      // options which use in mode create only.
    },

    optionsEdit() {
      // options which use in mode edit only.
    },
  },

};
