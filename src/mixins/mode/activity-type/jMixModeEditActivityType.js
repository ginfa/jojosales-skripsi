/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapState, mapActions } from 'vuex';

export const jMixModeEditActivityType = {
  beforeMount() {
    if (this.mode === 'edit') {
      this.modeEdit();
    }
  },
  methods: {
    ...mapActions('jStoreActivityType', ['getActivityTypeDetail']),
    modeEdit() {
      const activityTypeId = {
        id: Number(this.$route.params.idActivityType),
      };

      // get detail
      this.getActivityTypeDetail(activityTypeId);
    },
  },

  computed: {
    ...mapState('jStoreActivityType', ['activity_type_state']),

    // is_used_transaction_edit() {
    //   if (this.period_data.detail_period !== null) {
    //     return this.period_data.detail_period.is_used_transaction;
    //   }
    //   return false;
    // },

  },

  watch: {
    'activity_type_state.activity_type_detail'(newVal) {
      const activityTypeData = {
        id: newVal.id,
        name: newVal.name,
        description: newVal.description,
        form_id: newVal.form_id,
        policy_id: newVal.activity_type_policy_block_id,
        reminder_id: newVal.activity_type_reminder_block_id,
        icon: newVal.icon,
      };
      this.activity_type_data = activityTypeData;
    },
  },
};
