/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapState, mapActions } from 'vuex';
import constant from '../../../constant/constant';

export const jMixModeEditPolicy = {
  beforeMount() {
    if (this.mode === 'edit') {
      this.modeEdit();
    }
  },
  methods: {
    ...mapActions('jStorePolicy', ['getPolicyDetail']),
    modeEdit() {
      const policyId = {
        id: Number(this.$route.params.idPolicy),
      };

      // get detail
      this.getPolicyDetail(policyId);
    },
  },

  computed: {
    ...mapState('jStorePolicy', ['policy_state']),

    // is_used_transaction_edit() {
    //   if (this.period_data.detail_period !== null) {
    //     return this.period_data.detail_period.is_used_transaction;
    //   }
    //   return false;
    // },

  },

  watch: {
    'policy_state.policy_detail'(newVal) {
      const policies = [
        {
          policy: constant.PolicyWinningActivity,
          included: false,
        },
        {
          policy: constant.PolicyRequiresProduct,
          included: false,
        },
        {
          policy: constant.PolicyRequiresDocumentUpload,
          included: false,
        },
        {
          policy: constant.PolicyCloseActivity,
          included: false,
        },
        {
          policy: constant.PolicyRequiresDocumentTemplate,
          value: null,
          included: false,
        },
        {
          policy: constant.PolicyMaxNextActivity,
          type: null,
          value: 0,
          included: false,
        },
      ];

      newVal.policies.forEach((element) => {
        const policy = policies.filter(p => Number(p.policy) === Number(element.policy))[0];
        policy.included = true;
        if (Number(element.policy) === constant.PolicyRequiresDocumentTemplate ||
        Number(element.policy) === constant.PolicyMaxNextActivity) {
          policy.value = element.value;
          if (Number(element.policy) === constant.PolicyMaxNextActivity) {
            policy.type = element.type;
          }
        }
      });

      const policyData = {
        id: newVal.id,
        name: newVal.name,
        description: newVal.description,
        policies,
      };

      this.policy_data = policyData;
    },
  },
};
