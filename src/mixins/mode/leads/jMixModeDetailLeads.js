/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapState, mapActions } from 'vuex';

export const jMixModeDetailLeads = {
  beforeMount() {
    if (this.mode === 'detail') {
      this.modeDetail();
    }
  },
  methods: {
    ...mapActions('jStoreLeads', ['getLeadsDetail']),
    modeDetail() {
      const leadsId = {
        id: Number(this.$route.params.idLeads),
      };

      // get detail
      this.getLeadsDetail(leadsId);
    },
  },

  computed: {
    ...mapState('jStoreLeads', ['leads_state']),
  },

  watch: {
    'leads_state.leads_detail'(newVal) {
      const leadsData = {
        id: newVal.id,
        name: newVal.name,
        legal_name: newVal.legal_name,
        leads_company_group_id: newVal.leads_company_group_id,
        phone_no: newVal.phone_no,
        lead_source: newVal.leads_source_id,
        industry_id: newVal.industry_id,
        address: newVal.address,
        no_of_employee: newVal.no_of_employee,
        city_id: newVal.city_id,
        province_id: newVal.province_id,
        country_id: newVal.country_id,
        pic: newVal.pic,
      };
      this.leads_data = leadsData;
    },
  },
};
