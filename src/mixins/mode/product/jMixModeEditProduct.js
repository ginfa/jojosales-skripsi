/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapState, mapActions } from 'vuex';

export const jMixModeEditProduct = {
  beforeMount() {
    if (this.mode === 'edit') {
      this.modeEdit();
    }
  },
  methods: {
    ...mapActions('jStoreProduct', ['getProductDetail']),
    modeEdit() {
      const productId = {
        id: Number(this.$route.params.idProduct),
      };

      // get detail
      this.getProductDetail(productId);
    },
  },

  computed: {
    ...mapState('jStoreProduct', ['product_state']),

    // is_used_transaction_edit() {
    //   if (this.period_data.detail_period !== null) {
    //     return this.period_data.detail_period.is_used_transaction;
    //   }
    //   return false;
    // },

  },

  watch: {
    'product_state.product_detail'(newVal) {
      const subscription = {
        type: null,
        value: 1,
      };
      const productData = {
        id: newVal.id,
        name: newVal.name,
        description: newVal.description,
        price: newVal.price,
        unit_of_measure_id: newVal.unit_of_measure_id,
        currency: newVal.currency,
        icon: newVal.icon,
        type: newVal.type,
        subscription: (newVal.type === 1 ? newVal.subscription : subscription),
      };
      this.product_data = productData;
    },
  },
};
