/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapState, mapActions } from 'vuex';

export const jMixModeEditSource = {
  beforeMount() {
    if (this.mode === 'edit') {
      this.modeEdit();
    }
  },
  methods: {
    ...mapActions('jStoreSource', ['getSourceDetail']),
    modeEdit() {
      const periodId = {
        id: Number(this.$route.params.idSource),
      };

      // get detail
      this.getSourceDetail(periodId);
    },
  },

  computed: {
    ...mapState('jStoreSource', ['source_state']),
  },

  watch: {
    'source_state.source_detail'(newVal) {
      const sourceData = {
        id: newVal.id,
        name: newVal.name,
        icon: newVal.icon,
      };
      this.source_data = sourceData;
    },
  },
};
