/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapState, mapActions } from 'vuex';

export const jMixModeDetailOpportunity = {
  beforeMount() {
    if (this.mode === 'detail') {
      this.modeDetail();
    }
  },
  methods: {
    ...mapActions('jStoreOpportunity', ['getOpportunityDetail']),
    modeDetail() {
      const opportunityId = {
        id: Number(this.$route.params.idOpportunity),
      };

      // get detail
      this.getOpportunityDetail(opportunityId);
    },
  },

  computed: {
    ...mapState('jStoreOpportunity', ['opportunity_state']),
  },

  watch: {
    'opportunity_state.opportunity_detail'(newVal) {
      const opportunityData = {
        id: newVal.id,
        name: newVal.name === '' ? '-' : newVal.name,
        leads_name: newVal.leads_name === '' ? '-' : newVal.leads_name,
        product_list: (typeof newVal.activity !== 'undefined') ? newVal.activity.product : [],
        description: newVal.description === '' ? '-' : newVal.description,
        sales: (newVal.user !== 'undefined') ? `${newVal.user.profile.first_name} ${newVal.user.profile.last_name}` : '',
        pic_list: (typeof newVal.activity !== 'undefined') ? newVal.activity.pic : [],
      };
      this.opportunity_data = opportunityData;
    },
  },
};
