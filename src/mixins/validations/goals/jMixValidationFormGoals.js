/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { required, numeric } from 'vuelidate/lib/validators';

export const jMixValidationFormGoals = {
  validations() {
    return {
      goal_data: {
        name: {
          required,
        },
        activity_type_id: {
          required,
        },
        type: {
          required,
        },
        value: {
          required,
          numeric,
        },
        period_type: {
          required,
        },
        division: {
          required,
        },
      },
    };
  },
};
