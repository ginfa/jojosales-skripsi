/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { required } from 'vuelidate/lib/validators';

export const jMixValidationFormLeads = {
  validations() {
    return {
      leads_data: {
        name: {
          required,
        },
        legal_name: {
          required,
        },
        lead_source: {
          required,
        },
        leads_company_group_id: {
          required,
        },
        industry_id: {
          required,
        },
        address: {
          required,
        },
        no_of_employee: {
          required,
        },
      },
      pic_data: {
        name: {
          required,
        },
        phone_no: {
          required,
        },
        title: {
          required,
        },
        email: {
          required,
        },
      },
    };
  },
};
