/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { required, requiredIf } from 'vuelidate/lib/validators';
// import constant from '../../../constant/constant';

export const jMixValidationFormStage = {
  validations() {
    return {
      stage_data: {
        name: {
          required,
        },
        activity_type: {
          required,
        },
        color: {
          required,
        },
        icon: {
          required,
        },
      },
    };
  },
};
