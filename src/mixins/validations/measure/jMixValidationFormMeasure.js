/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { required } from 'vuelidate/lib/validators';

export const jMixValidationFormMeasure = {
  validations() {
    return {
      measure_data: {
        name: {
          required,
        },
        description: {
          required,
        },
      },
    };
  },
};
