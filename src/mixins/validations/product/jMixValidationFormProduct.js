/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { required, requiredIf } from 'vuelidate/lib/validators';
import constant from '../../../constant/constant';

export const jMixValidationFormProduct = {
  validations() {
    return {
      product_data: {
        name: {
          required,
        },
        description: {
          required,
        },
        price: {
          required,
        },
        unit_of_measure_id: {
          required,
        },
        currency: {
          required,
        },
        type: {
          required,
        },
        subscription: {
          type: {
            requiredIf: requiredIf(() =>
              this.product_data.type === constant.ProductTypeSubscription,
            ),
          },
        },
        // icon validation
      },
    };
  },
};
