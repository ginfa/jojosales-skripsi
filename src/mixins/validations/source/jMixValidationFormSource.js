/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { required } from 'vuelidate/lib/validators';

export const jMixValidationFormSource = {
  validations() {
    return {
      source_data: {
        name: {
          required,
        },
        icon: {
          required,
        },
      },
    };
  },
};
