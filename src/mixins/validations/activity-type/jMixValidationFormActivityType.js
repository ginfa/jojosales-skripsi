/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { required } from 'vuelidate/lib/validators';

export const jMixValidationFormActivityType = {
  validations() {
    return {
      activity_type_data: {
        name: {
          required,
        },
        description: {
          required,
        },
        form_id: {

        },
        icon: {
          required,
        },
        policy_id: {
          required,
        },
        reminder_id: {
          required,
        },
      },
    };
  },
};
