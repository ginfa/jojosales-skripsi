/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions, mapState } from 'vuex';

export const jMixModelDeleteMeasures = {
  computed: {
    delete_data() {
      const ids = [];
      if (this.selectedRows.length > 0) {
        this.selectedRows.forEach((row) => {
          const dataRow = {
            ids: [row.id],
          };
          ids.push(dataRow);
        });
        return ids;
      }
      return {
        ids,
      };
    },
  },

  methods: {
    ...mapActions('jStoreMeasure', ['deleteMeasure']),

    deleteMeasures() {
      if (this.delete_data !== null) {
        const text = `You want to delete ${this.measures_name}`;

        this.$swal({
          title: 'Are you sure?',
          text,
          type: 'question',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
        }).then((result) => {
          if (result.value) {
            // this.deleteMeasure(this.delete_data);
            this.$refs.deleteMeasure.toggleIsVisible();
          }
        });
      }
    },
  },

};

export const jMixModelDeleteMeasure = {
  computed: {
    ...mapState('jStoreMeasure', ['measure_state']),

    delete_data() {
      if (this.measure_state.measure_detail !== null) {
        return {
          ids: [Number(this.$route.params.idMeasure)],
        };
      }
      return null;
    },
  },

  methods: {
    ...mapActions('jStoreMeasure', ['deleteMeasure']),

    deleteData() {
      if (!this.is_used_transaction_edit) {
        this.$swal({
          title: 'Are you sure?',
          text: 'to delete this measure',
          type: 'warning',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
        }).then((result) => {
          if (result.value) {
            this.deleteMeasure(this.delete_data);
          }
        });
      }
    },
  },

};
