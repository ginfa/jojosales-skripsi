/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions } from 'vuex';

export const jMixModelEditMeasure = {
  computed: {
    generateData() {
      if (this.measure_data !== null) {
        const genData = {
          id: this.measure_data.id,
          name: this.measure_data.name,
          description: this.measure_data.description,
          icon: 'https://corp.jojonomic.com//assets/img/times/leave/leave_business.png',
        };
        return genData;
      }
      return null;
    },
  },
  methods: {
    ...mapActions('jStoreMeasure', ['updateMeasure']),
    sendData() {
      this.updateMeasure(this.generateData);
    },
  },
};
