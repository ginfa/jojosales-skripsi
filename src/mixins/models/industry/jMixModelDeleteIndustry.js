/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions, mapState } from 'vuex';

export const jMixModelDeleteIndustry = {
  computed: {
    ...mapState('jStoreIndustry', ['industry_state']),

    delete_data() {
      if (this.industry_state.industry_detail !== null) {
        return {
          ids: [Number(this.$route.params.idIndustry)],
        };
      }
      return null;
    },
  },

  methods: {
    ...mapActions('jStoreIndustry', ['deleteIndustry']),

    deleteData(val) {
      if (!this.is_used_transaction_edit) {
        this.$swal({
          title: 'Are you sure?',
          text: 'to delete this industry',
          type: 'warning',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
        }).then((result) => {
          if (result.value) {
            if (typeof val !== 'object') {
              this.deleteIndustry({ ids: [Number(val)] });
            } else {
              this.deleteIndustry(this.delete_data);
            }
          }
        });
      }
    },
  },

};
