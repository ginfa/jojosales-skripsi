/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions } from 'vuex';

export const jMixModelEditIndustry = {
  computed: {
    generateData() {
      if (this.industry_data !== null) {
        const genData = {
          id: this.industry_data.id,
          name: this.industry_data.name,
        };
        return genData;
      }
      return null;
    },
  },
  methods: {
    ...mapActions('jStoreIndustry', ['updateIndustry']),

    sendData() {
      this.updateIndustry(this.generateData);
    },
  },
};
