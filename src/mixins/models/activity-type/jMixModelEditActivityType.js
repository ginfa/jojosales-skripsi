/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions } from 'vuex';

export const jMixModelEditActivityType = {
  computed: {
    generateData() {
      if (this.activity_type_data !== null) {
        const genData = {
          id: this.activity_type_data.id,
          name: this.activity_type_data.name,
          description: this.activity_type_data.description,
          activity_type_policy_block_id: this.activity_type_data.policy_id,
          activity_type_reminder_block_id: this.activity_type_data.reminder_id,
          form_id: this.activity_type_data.form_id,
          icon: this.activity_type_data.icon,
        };
        return genData;
      }
      return null;
    },
  },
  methods: {
    ...mapActions('jStoreActivityType', ['updateActivityType']),
    sendData() {
      this.updateActivityType(this.generateData);
    },
  },
};
