/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions } from 'vuex';

export const jMixModelEditGoals = {
  computed: {
    generateData() {
      if (this.goal_data !== null) {
        const genData = {
          id: this.goal_data.id,
          name: this.goal_data.name,
          activity_type_id: this.goal_data.activity_type_id,
          type: this.goal_data.type,
          value: this.goal_data.value,
          period_type: this.goal_data.period_type,
          division_id: this.goal_data.division,
        };
        return genData;
      }
      return null;
    },
  },
  methods: {
    ...mapActions('jStoreGoals', ['updateGoal']),

    sendData() {
      this.updateGoal(this.generateData);
    },
  },
};
