/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions, mapState } from 'vuex';

export const jMixModelDeleteGoal = {
  computed: {
    ...mapState('jStoreGoals', ['goal_state']),

    delete_data() {
      if (this.goal_state.goal_detail !== null) {
        return {
          ids: [Number(this.$route.params.idGoal)],
        };
      }
      return null;
    },
  },

  methods: {
    ...mapActions('jStoreGoals', ['deleteGoal']),

    deleteData(val) {
      if (!this.is_used_transaction_edit) {
        this.$swal({
          title: 'Are you sure?',
          text: 'to delete this goal',
          type: 'warning',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
        }).then((result) => {
          if (result.value) {
            if (typeof val !== 'object') {
              this.deleteGoal({ ids: [Number(val)] });
            } else {
              this.deleteGoal(this.delete_data);
            }
          }
        });
      }
    },
  },

};
