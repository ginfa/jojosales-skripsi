/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions, mapState } from 'vuex';

export const jMixModelDeleteSource = {
  computed: {
    ...mapState('jStoreSource', ['source_state']),

    delete_data() {
      if (this.source_state.source_detail !== null) {
        return {
          ids: [Number(this.$route.params.idSource)],
        };
      }
      return null;
    },
  },

  methods: {
    ...mapActions('jStoreSource', ['deleteSource']),

    deleteData(val) {
      if (!this.is_used_transaction_edit) {
        this.$swal({
          title: 'Are you sure?',
          text: 'to delete this source',
          type: 'warning',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
        }).then((result) => {
          if (result.value) {
            if (typeof val !== 'object') {
              this.deleteSource({ ids: [Number(val)] });
            } else {
              this.deleteSource(this.delete_data);
            }
          }
        });
      }
    },
  },

};
