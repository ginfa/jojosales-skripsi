/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions } from 'vuex';

export const jMixModelEditSource = {
  computed: {
    generateData() {
      if (this.source_data !== null) {
        const genData = {
          id: this.source_data.id,
          name: this.source_data.name,
          icon: this.source_data.icon,
        };
        return genData;
      }
      return null;
    },
  },
  methods: {
    ...mapActions('jStoreSource', ['updateSource']),

    sendData() {
      this.updateSource(this.generateData);
    },
  },
};
