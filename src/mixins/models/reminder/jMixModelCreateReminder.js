/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions } from 'vuex';

export const jMixModelCreateReminder = {
  computed: {
    generateData() {
      if (this.reminder_data !== null) {
        const genData = {
          id: this.reminder_data.id,
          name: this.reminder_data.name,
          description: this.reminder_data.description,
          reminders: this.reminder_data.reminders,
        };
        return genData;
      }
      return null;
    },
  },
  methods: {
    ...mapActions('jStoreReminder', ['createReminder']),

    sendData() {
      this.createReminder(this.generateData);
    },
  },
};
