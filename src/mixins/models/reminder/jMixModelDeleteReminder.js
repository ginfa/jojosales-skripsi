/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions, mapState } from 'vuex';

export const jMixModelDeleteReminders = {
  computed: {
    delete_data() {
      const ids = [];
      if (this.selectedRows.length > 0) {
        this.selectedRows.forEach((row) => {
          const dataRow = {
            ids: [row.id],
          };
          ids.push(dataRow);
        });
        return ids;
      }
      return {
        ids,
      };
    },
  },

  methods: {
    ...mapActions('jStoreReminder', ['deleteReminder']),

    deleteReminders() {
      if (this.delete_data !== null) {
        const text = `You want to delete ${this.reminders_name}`;

        this.$swal({
          title: 'Are you sure?',
          text,
          type: 'question',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
        }).then((result) => {
          if (result.value) {
            // this.deleteReminder(this.delete_data);
            this.$refs.deleteReminder.toggleIsVisible();
          }
        });
      }
    },
  },

};

export const jMixModelDeleteReminder = {
  computed: {
    ...mapState('jStoreReminder', ['reminder_state']),

    delete_data() {
      if (this.reminder_state.reminder_detail !== null) {
        return {
          ids: [Number(this.$route.params.idReminder)],
        };
      }
      return null;
    },
  },

  methods: {
    ...mapActions('jStoreReminder', ['deleteReminder']),

    deleteData() {
      if (!this.is_used_transaction_edit) {
        this.$swal({
          title: 'Are you sure?',
          text: 'to delete this reminder',
          type: 'warning',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
        }).then((result) => {
          if (result.value) {
            this.deleteReminder(this.delete_data);
          }
        });
      }
    },
  },

};
