/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions, mapState } from 'vuex';

export const jMixModelDeleteProducts = {
  computed: {
    delete_data() {
      const ids = [];
      if (this.selectedRows.length > 0) {
        this.selectedRows.forEach((row) => {
          const dataRow = {
            ids: [row.id],
          };
          ids.push(dataRow);
        });
        return ids;
      }
      return ids;
    },
  },

  methods: {
    ...mapActions('jStoreProduct', ['deleteProduct']),

    deleteProducts() {
      if (this.delete_data.length > 0) {
        const text = `You want to delete ${this.products_name}`;

        this.$swal({
          title: 'Are you sure?',
          text,
          type: 'question',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
        }).then((result) => {
          if (result.value) {
            // this.deleteProduct(this.delete_data);
            this.$refs.deleteProduct.toggleIsVisible();
          }
        });
      }
    },
  },

};

export const jMixModelDeleteProduct = {
  computed: {
    ...mapState('jStoreProduct', ['product_state']),

    delete_data() {
      if (this.product_state.product_detail !== null) {
        return {
          product: {
            ids: [Number(this.$route.params.idProduct)],
          },
        };
      }
      return null;
    },
  },

  methods: {
    ...mapActions('jStoreProduct', ['deleteProduct']),

    deleteData() {
      if (!this.is_used_transaction_edit) {
        this.$swal({
          title: 'Are you sure?',
          text: 'to delete this product',
          type: 'warning',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
        }).then((result) => {
          if (result.value) {
            this.deleteProduct(this.delete_data.product);
          }
        });
      }
    },
  },

};
