/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions } from 'vuex';

export const jMixModelCreateProduct = {
  computed: {
    generateData() {
      if (this.product_data !== null) {
        const genData = {
          id: this.product_data.id,
          name: this.product_data.name,
          description: this.product_data.description,
          price: this.product_data.price,
          unit_of_measure_id: this.product_data.unit_of_measure_id,
          currency: this.product_data.currency,
          icon: (this.product_data.icon === null ? '' : this.product_data.icon),
          type: this.product_data.type,
          subscription: (this.product_data.type === 1 ? this.product_data.subscription : null),
        };
        return genData;
      }
      return null;
    },
  },
  methods: {
    ...mapActions('jStoreProduct', ['createProduct']),

    sendData() {
      this.createProduct(this.generateData);
    },
  },
};
