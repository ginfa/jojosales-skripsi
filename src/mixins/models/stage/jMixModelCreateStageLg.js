/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand,camelcase */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing,max-len */
import { mapActions } from 'vuex';

export const jMixModelCreateStageLg = {
  computed: {
    generateData() {
      if (this.stage_list.length > 0) {
        const genData = [];
        this.stage_list.forEach((stage) => {
          if (!(stage.is_delete && stage.id === 0)) {
            const activity_type = stage.activity_type;
            // stage.activity_type.forEach((element) => {
            //   const type = {
            //     id: element,
            //     name: this.activity_type_options.filter(a => (a.id === element))[0].name,
            //   };
            //   activity_type.push(type);
            // });
            const data = {
              id: stage.id,
              name: stage.name,
              description: stage.description,
              activity_type,
              is_delete: stage.is_delete,
              position: stage.position,
              type: stage.type,
              icon: stage.icon,
              color: stage.color,
            };
            genData.push(data);
          }
        });
        return genData;
      }
      return null;
    },
  },
  methods: {
    ...mapActions('jStoreStage', ['createStage']),

    sendData() {
      this.createStage(this.generateData);
    },
  },
};
