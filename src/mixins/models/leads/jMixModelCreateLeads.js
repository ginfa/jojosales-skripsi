/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions } from 'vuex';

export const jMixModelCreateLeads = {
  computed: {
    generateData() {
      if (this.leads_data !== null) {
        const genData = {
          id: this.leads_data.id,
          name: this.leads_data.name,
          legal_name: this.leads_data.legal_name,
          leads_company_group_id: this.leads_data.leads_company_group_id,
          phone_no: this.leads_data.phone_no,
          lead_source: this.leads_data.lead_source,
          industry_id: this.leads_data.industry_id,
          address: this.leads_data.address,
          no_of_employee: this.leads_data.no_of_employee,
          city_id: this.leads_data.city_id,
          province_id: this.leads_data.province_id,
          country_id: this.leads_data.country_id,
          pic: this.leads_data.pic,
        };
        return genData;
      }
      return null;
    },
  },
  methods: {
    ...mapActions('jStoreLeads', ['createLeads']),

    sendData() {
      this.createLeads(this.generateData);
    },
  },
};
