/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions, mapState } from 'vuex';

export const jMixModelEditPolicy = {
  computed: {
    generateData() {
      if (this.policy_data !== null) {
        const policies = [];

        this.policy_data.policies.forEach((element) => {
          if (element.included) {
            const policy = {
              policy: element.policy,
              type: (typeof element.type !== 'undefined') ? element.type : 1,
              value: (typeof element.value !== 'undefined') ? element.value : 0,
            };

            policies.push(policy);
          }
        });

        const genData = {
          id: this.policy_data.id,
          name: this.policy_data.name,
          description: this.policy_data.description,
          policies,
        };

        return genData;
      }
      return null;
    },
  },
  methods: {
    ...mapActions('jStorePolicy', ['updatePolicy', 'setDefaultDataChanged']),
    sendData() {
      this.updatePolicy(this.generateData);
    },
  },
};
