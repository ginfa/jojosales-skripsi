/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */
import { mapActions, mapState } from 'vuex';

export const jMixModelDeletePolicies = {
  computed: {
    delete_data() {
      const ids = [];
      if (this.selectedRows.length > 0) {
        this.selectedRows.forEach((row) => {
          const dataRow = {
            ids: [row.id],
          };
          ids.push(dataRow);
        });
        return ids;
      }
      return {
        ids,
      };
    },
  },

  methods: {
    ...mapActions('jStorePolicy', ['deletePolicy']),

    deletePolicies() {
      if (this.delete_data !== null) {
        const text = `You want to delete ${this.policies_name}`;

        this.$swal({
          title: 'Are you sure?',
          text,
          type: 'question',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
        }).then((result) => {
          if (result.value) {
            // this.deletePolicy(this.delete_data);
            this.$refs.deletePolicy.toggleIsVisible();
          }
        });
      }
    },
  },

};

export const jMixModelDeletePolicy = {
  computed: {
    ...mapState('jStorePolicy', ['policy_state']),

    delete_data() {
      if (this.policy_state.policy_detail !== null) {
        return {
          ids: [Number(this.$route.params.idPolicy)],
        };
      }
      return null;
    },
  },

  methods: {
    ...mapActions('jStorePolicy', ['deletePolicy']),

    deleteData() {
      if (!this.is_used_transaction_edit) {
        this.$swal({
          title: 'Are you sure?',
          text: 'to delete this policy',
          type: 'warning',
          showCancelButton: true,
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
        }).then((result) => {
          if (result.value) {
            this.deletePolicy(this.delete_data);
          }
        });
      }
    },
  },

};
