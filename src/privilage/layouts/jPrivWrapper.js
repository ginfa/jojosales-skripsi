/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */

export const jPrivWrapper = {
  computed: {
    productsDetails() {
      // const packageData = this.package ? this.package : [];
      return [
        {
          name: 'Jojo Pro',
          href: null,
          hrefout: '/',
          // eslint-disable-next-line
          img: require('@/assets/logo/logo-jojonomicpro.png'),
          isShow: true,
        },
      ];
    },
  },

  created() {
    const role = JSON.parse(localStorage.getItem('role'));
    const sidebarMenu = [
      {
        parent: {
          name: 'HOME',
          href: '/home',
          icon: 'file-text-o',
          isShow: true,
        },
      },
      {
        parent: {
          name: 'CONFIG',
          icon: 'file-text-o',
          isShow: true,
        },
        child: [
          {
            name: 'STAGE',
            href: '/config/stage',
            icon: 'calendar-minus-o',
            isShow: true,
          },
          {
            name: 'ACTIVITY TYPE',
            href: '/config/activity-type',
            icon: 'calendar-minus-o',
            isShow: true,
          },
          {
            name: 'PRODUCT',
            href: '/config/product',
            icon: 'calendar-minus-o',
            isShow: true,
          },
          {
            name: 'SOURCE',
            href: '/config/source',
            icon: 'calendar-minus-o',
            isShow: true,
          },
          {
            name: 'GOALS',
            href: '/config/goals',
            icon: 'calendar-minus-o',
            isShow: true,
          },
          {
            name: 'INDUSTRY',
            href: '/config/industry',
            icon: 'calendar-minus-o',
            isShow: true,
          },
        ],
      },
      {
        parent: {
          name: 'SALES',
          icon: 'file-text-o',
          isShow: true,
        },
        child: [
          {
            name: 'DAILY LOG',
            href: '/sales/daily-log',
            icon: 'calendar-minus-o',
            isShow: true,
          },
          {
            name: 'PIPELINE',
            href: '/sales/pipeline',
            icon: 'calendar-minus-o',
            isShow: true,
          },
          {
            name: 'LEADS',
            href: '/sales/leads',
            icon: 'calendar-minus-o',
            isShow: true,
          },
          {
            name: 'OPPORTUNITY',
            href: '/sales/opportunity',
            icon: 'calendar-minus-o',
            isShow: true,
          },
        ],
      },
      {
        parent: {
          name: 'REPORT',
          href: '/report',
          icon: 'file-text-o',
          isShow: true,
        },
      },
    ];
    this.menusDetails.push(...sidebarMenu);
  },
};
